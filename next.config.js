const path = require("path");
const withPlugins = require("next-compose-plugins");
const withOffline = require("next-offline");

const plugins = [
  [
    withOffline,
    {
      workboxOpts: {
        runtimeCaching: [
          {
            urlPattern: /.(png|jpe?g)$/,
            handler: "CacheFirst",
          },
        ],
      },
    },
  ],
];

module.exports = withPlugins(plugins, {
  env: {
    API: "https://cloud.waves.mn/gegee-images/",
  },
  webpack(config) {
    config.resolve.alias["@"] = path.join(__dirname, "src");
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(js|ts)x?$/,
      },
      use: ["@svgr/webpack"],
    });
    return config;
  },
});
