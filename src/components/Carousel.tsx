import styled from "styled-components";

export const CarouselItem = styled.div`
  width: 100%;
  position: relative;
  background-size: cover;
  background-position: center;
  display: flex;
  justify-content: center;
  background-color: rgb(230, 230, 230);
  background-image: url(${(props) => props.src});
  height: calc(100vh - 440px);
  margin-top: 69px;
  @media (max-width: 1200px) {
    height: calc(100vh - 434px);
  }
  @media (max-width: 960px) {
    margin-top: 49px;
    height: 30vh;
  }
`;
export const CarouselButtonLayout = styled.div`
  width: 100%;
  height: calc(100% - 10vh);
  display: flex;
  align-items: flex-end;
  padding: 5vh 24px;
  max-width: 912px;
  justify-content: ${(props) => props.justify};
  @media (max-width: 960px) {
    height: calc(100% - 8vh);
    padding: 4vh 24px;
    justify-content: center;
  }
`;

export const CarouselBtn = styled.div`
  width: auto;
  width: 160px;
  display: flex;
  cursor: pointer;
  padding: 12px 0;
  border-radius: 20px;
  justify-content: center;
  background-color: white;
  color: rgb(117, 117, 117);
  i {
    margin-left: 6px;
    color: orange;
  }
  @media (max-width: 960px) {
    font-size: 14px;
    opacity: 0.95;
    width: 120px;
    padding: 8px 0;
    right: calc(50% - 65px);
  }
`;

export const CoverLayout = styled.div`
  width: 100%;
  position: relative;
  background-size: cover;
  background-position: center;
  background-image: url(${(props) => props.src});
  background-color: #dedede;
  background-repeat: no-repeat;
  height: calc(100vh - 468px);
  margin-top: 69px;
  @media (max-width: 1200px) {
    height: calc(100vh - 462px);
  }
  @media (max-width: 960px) {
    margin-top: 49px;
    height: 20vh;
  }
`;
export const Image = styled.div`
  width: 100%;
  height: auto;
  background-size: contain;
  margin-top: 69px;
  @media (max-width: 960px) {
    margin-top: 49px;
  }
`;

export const PostImage = styled.img`
  width: 100%;
  background-color: #dedede;
  object-fit: contain;
`;
