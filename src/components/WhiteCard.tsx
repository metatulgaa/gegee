import styled from "styled-components";

export const WhitCardLayout = styled.div`
  padding: 48px 24px;
  background-color: white;
  font-family: "Magistral";
  color: rgb(117, 117, 117);
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  height: calc(100% - 96px);
  img {
    height: 150px;
    padding-right: 50px;
  }
  h1 {
    text-align: center;
    font-size: 25px;
    font-weight: 300;
    text-transform: uppercase;
    margin-top: 36px;
  }
  label {
    text-align: justify;
    line-height: 18px;
  }
  @media (max-width: 960px) {
    img {
      height: 120px;
    }
    h1 {
      font-size: 16px;
    }
    label {
      font-size: 14px;
    }
  }
`;

const WhiteCard = ({
  title,
  description,
  imgPath,
}: {
  title: string;
  description: string;
  imgPath: string;
}) => {
  return (
    <WhitCardLayout>
      <img loading="lazy" src={imgPath} alt="зураг" />
      <div>
        <h1>{title}</h1>
      </div>
      <div style={{ textAlign: "justify" }}>
        <label>{description}</label>
      </div>
    </WhitCardLayout>
  );
};

export default WhiteCard;
