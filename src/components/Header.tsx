import Link from "next/link";
import styled from "styled-components";

import { Dispatch } from "react";
import { ReactSVG } from "react-svg";
import { Container } from "@material-ui/core";

import { CardLayout } from "./Card";
import { Categories } from "../hooks/useCategories";
import { StaticCategories } from "../utils/categories";

const HeaderLayout = styled.div`
  background-color: #ffffff;
  width: 100%;
  padding: 20px 0;
  position: sticky;
  top: 0;
  z-index: 3;
  img {
    cursor: pointer;
  }
  a {
    font-family: "Hermes";
    font-size: 15px;
    text-transform: uppercase;
    color: rgb(117, 117, 117);
    margin: 0 8px;
    cursor: pointer;
    text-decoration: none;

    &:last-child {
      margin-right: 0;
    }
  }
  @media (max-width: 768px) {
    display: none;
  }
`;

const Menu = styled.div`
  position: absolute;
  top: 36px;
  left: 0;
  right: auto;
  z-index: 10;
  display: none;
  align-items: center;
  padding: 10px 5px;
  border-radius: 10px;
  background-color: #fff;
  box-shadow: 0 0.125rem 0.5rem rgba(0, 0, 0, 0.1),
    0 0.0625rem 0.125rem rgba(0, 0, 0, 0.1);

  &::before {
    content: "";
    position: absolute;
    width: 0;
    height: 0;
    bottom: 100%;
    left: 45px;
    transform: translate(-50%, 0);
    border: 15px solid transparent;
    border-top: none;
    border-bottom-color: #fff;
    filter: drop-shadow(0 -0.0625rem 0.0625rem rgba(0, 0, 0, 0.1));
  }
`;

export const FlexLayout = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
`;

const Header = ({
  show,
  setShow,
  categories,
}: {
  show: number;
  setShow: Dispatch<number>;
  categories: Categories;
}) => {
  return (
    <HeaderLayout>
      <Container maxWidth="md">
        <FlexLayout>
          <Link href="/">
            <img
              loading="lazy"
              src="/logo.png"
              alt="logo"
              style={{ width: 90 }}
            />
          </Link>
          <FlexLayout style={{ justifyContent: "flex-end" }}>
            <Link href="/lessons">
              <a style={{ color: show === 0 && "rgb(255, 166, 0)" }}>
                <span onMouseEnter={() => setShow(0)}>Сургалтууд</span>
                <div style={{ position: "relative", width: "100%" }}>
                  <div
                    style={{
                      position: "absolute",
                      background: "transparent",
                      height: 100,
                      width: "120%",
                    }}
                  >
                    <Menu style={{ display: show === 0 && "flex" }}>
                      {categories?.map((cat, index) => (
                        <Link
                          href={
                            cat.childrenCategories.length === 0
                              ? `/lessons/${cat._id}`
                              : `/lessons/child/${cat._id}`
                          }
                          key={index}
                        >
                          <div
                            style={{
                              width: `${100 / (categories?.length + 1)}%`,
                              padding: "0 12px",
                              borderRight:
                                categories?.length !== index &&
                                "1px solid rgb(230, 230, 230)",
                            }}
                          >
                            <CardLayout
                              style={{
                                border: "none",
                                paddingBottom: 6,
                              }}
                            >
                              <img
                                src="/assets/icons/english.svg"
                                style={{ height: 40 }}
                              />
                              {cat.name && (
                                <p style={{ whiteSpace: "pre" }}>{cat.name}</p>
                              )}
                            </CardLayout>
                          </div>
                        </Link>
                      ))}
                      <Link href="/">
                        <div
                          style={{
                            width: `${100 / (categories?.length + 1)}%`,
                            padding: "0 12px",
                          }}
                        >
                          <CardLayout
                            style={{
                              border: "none",
                              paddingBottom: 6,
                            }}
                          >
                            <img
                              src="/assets/icons/level.svg"
                              style={{ height: 40 }}
                            />
                            <p style={{ whiteSpace: "pre" }}>
                              Түвшин тогтоох тест
                            </p>
                          </CardLayout>
                        </div>
                      </Link>
                    </Menu>
                  </div>
                </div>
              </a>
            </Link>

            {StaticCategories.map((item) => (
              <Link href={item.path} key={item.key}>
                <a style={{ color: show === item.key && "rgb(255, 166, 0)" }}>
                  <span onMouseEnter={() => setShow(item.key)}>
                    {item.title}
                  </span>
                  <div style={{ position: "relative", width: "100%" }}>
                    <div
                      style={{
                        position: "absolute",
                        background: "transparent",
                        height: 100,
                        width: "120%",
                      }}
                    >
                      <Menu style={{ display: show === item.key && "flex" }}>
                        {item.submenu?.map((sub, index) => (
                          <Link href={sub.pathname} key={index}>
                            <div
                              style={{
                                width: `${100 / item.submenu.length}%`,
                                padding: "0 12px",
                                borderRight:
                                  item.submenu.length - 1 !== index &&
                                  "1px solid rgb(230, 230, 230)",
                              }}
                            >
                              <CardLayout
                                style={{
                                  border: "none",
                                  paddingBottom: 6,
                                }}
                              >
                                {/* <ReactSVG
                                  src={sub.imgPath}
                                  beforeInjection={(svg) => {
                                    svg.setAttribute("style", "height: 40px");
                                  }}
                                /> */}
                                <img src={sub.imgPath} style={{ height: 40 }} />
                                {sub.title && (
                                  <p style={{ whiteSpace: "pre" }}>
                                    {sub.title}
                                  </p>
                                )}
                              </CardLayout>
                            </div>
                          </Link>
                        ))}
                      </Menu>
                    </div>
                  </div>
                </a>
              </Link>
            ))}
          </FlexLayout>
        </FlexLayout>
      </Container>
    </HeaderLayout>
  );
};

export default Header;
