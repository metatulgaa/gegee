import { Grid } from "@material-ui/core";
import styled from "styled-components";
import { FlexLayout } from "./Header/Header";

export const CommentLayout = styled.div`
  color: rgb(117, 117, 117);

  padding-bottom: 12px;
  h3 {
    margin: 0;
  }
  label {
    display: inherit;
    margin: 6px 0;
  }
  span {
    font-size: 12px;
    color: rgb(255, 166, 0);
    margin-right: 12px;
    i {
      margin-right: 6px;
    }
  }
  a {
    cursor: pointer;
    font-size: 12px;
    color: rgb(255, 166, 0);
    margin-right: 12px;
    i {
      margin-right: 6px;
    }
  }
`;

export const Icon = styled.div`
  background-color: rgb(255, 166, 0);
  border-radius: 50%;
  margin-right: 24px;
  color: rgba(247, 247, 247);
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 30px;
  padding: 5px;
  width: 35px;
  height: 35px;
`;

export const CommentItem = ({ text, date }: { text: string; date: string }) => {
  return (
    <CommentLayout>
      <Grid container>
        <Grid item xs={2} sm={1}>
          <Icon>
            <i className="fa fa-user" />
          </Icon>
        </Grid>
        <Grid item xs={10} sm={11}>
          <h3>Зочин</h3>
          <label>{text}</label>
          <div>
            <span>
              <i className="fal fa-clock" />
              {date}
            </span>

            <a>
              <i className="fal fa-comment" />
              Хариулах
            </a>
          </div>
        </Grid>
      </Grid>
    </CommentLayout>
  );
};
