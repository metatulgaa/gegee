import styled from "styled-components";

export const NoDataLayout = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  font-size: 12px;
  text-transform: uppercase;
  padding: 5vh 0;
  color: rgb(117, 117, 117);
  text-align: center;
  line-height: 20px;
  i {
    font-size: 50px;
  }
`;

const NoData = () => {
  return (
    <NoDataLayout>
      <i className="fal fa-archive" />
      <p>
        одоогоор мэдээлэл
        <br /> байхгүй байна.
      </p>
    </NoDataLayout>
  );
};

export default NoData;
