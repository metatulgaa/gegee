import styled from "styled-components";

export const Section = styled.div`
  background-color: ${(props) => props.color};
  padding: ${(props) => props.padding};
  margin: ${(props) => props.margin};
  @media (max-width: 960px) {
    padding: 32px 0;
  }
`;
