import Link from "next/link";
import { Dispatch } from "react";
import { ReactSVG } from "react-svg";

import { StaticCategories } from "../../utils/categories";
import { CardLayout } from "../Card";
import { MenuLayout, Menu } from "./Header";

const StaticCatigories = ({
  show,
  setShow,
}: {
  show: number;
  setShow: Dispatch<number>;
}) => {
  return (
    <>
      {StaticCategories.map((item) => (
        <Link href={item.path} key={item.key}>
          <a style={{ color: show === item.key && "rgb(255, 166, 0)" }}>
            <span onMouseEnter={() => setShow(item.key)}>{item.title}</span>
            <div style={{ position: "relative", width: "100%" }}>
              <MenuLayout>
                <Menu style={{ display: show === item.key && "flex" }}>
                  {item.submenu?.map((sub, index) => (
                    <Link href={sub.pathname} key={index}>
                      <div
                        style={{
                          width: `${100 / item.submenu.length}%`,
                          padding: "0px 6px",
                          borderRight:
                            item.submenu.length - 1 !== index &&
                            "1px solid rgb(230, 230, 230)",
                        }}
                      >
                        <CardLayout
                          style={{
                            border: "none",
                            width: `calc(100% - ${48}px)`,
                            padding: "12px 24px",
                            borderRadius: 10,
                          }}
                        >
                          <ReactSVG
                            src={sub.imgPath}
                            beforeInjection={(svg) => {
                              svg.setAttribute(
                                "style",
                                "width: 50px; height: 50px;"
                              );
                            }}
                          />
                          {sub.title && (
                            <p
                              style={{
                                whiteSpace: "pre",
                                margin: "6px 0 0 0",
                                fontSize: 14,
                              }}
                            >
                              {sub.title}
                            </p>
                          )}
                        </CardLayout>
                      </div>
                    </Link>
                  ))}
                </Menu>
              </MenuLayout>
            </div>
          </a>
        </Link>
      ))}
    </>
  );
};

export default StaticCatigories;
