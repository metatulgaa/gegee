import Link from "next/link";
import { useState } from "react";
import styled from "styled-components";

const HeaderLayout = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  display: none;
  align-items: center;
  width: calc(100% - 32px);
  padding: 12px 16px;
  background-color: rgb(255, 255, 255, 0.98);
  justify-content: space-between;
  align-items: center;
  z-index: 10;
  border-bottom: 1px solid rgb(230, 230, 230);
  img {
    cursor: pointer;
    height: 25px;
  }
  a {
    color: orange;
    font-size: 23px;
  }
  @media (max-width: 960px) {
    display: flex;
  }
`;

const SideBar = styled.div`
  display: none;
  height: 100%;
  width: auto;
  position: fixed;
  z-index: 12;
  top: 0;
  left: -300px;
  background-color: #fff;
  overflow-x: hidden;
  transition: 0.5s;
  box-shadow: 0px 0px 24px #00000060;
  a {
    font-size: 14px;
    padding: 14px 16px;
    font-weight: bold;
    color: rgb(117, 117, 117);
    display: block;
    text-decoration: none;
    i {
      color: orange;
      margin-right: 12px;
    }
  }
  @media (max-width: 960px) {
    display: block;
  }
`;

const CloseBtn = styled.div`
  text-align: right;
  margin-left: 16px;
  color: orange;
  font-size: 20px;
  z-index: 1000;
`;

const Shadow = styled.div`
  width: 100%;
  position: fixed;
  height: 100vh;
  background-color: #00000080;
  z-index: 11;
  display: none;
  transition: display 0.5s;
`;

const CloseBtnLayout = styled.div`
  padding: 12px 16px;
`;

export const SocialLayout = styled.div`
  display: flex;
  align-items: center;
  img {
    height: 25px;
    margin: 0 8px;
  }
  i {
    margin-right: 12px;
  }
  a {
    padding: 0;
  }
`;

const MobileHeader = () => {
  const [show, setShow] = useState(false);
  return (
    <>
      <Shadow
        style={{ display: show ? "block" : "none" }}
        onClick={() => setShow(false)}
      />
      <HeaderLayout>
        <a onClick={() => setShow(true)}>
          <i className="fa fa-bars" />
        </a>
        <SocialLayout>
          <a href="https://www.facebook.com/GEGEEteam/" target="_blank">
            <img loading="lazy" src="/assets/social/facebook.png" />
          </a>
          <a
            href="https://www.instagram.com/gegee.learnsmarter/"
            target="_blank"
          >
            <img loading="lazy" src="/assets/social/instagram.png" />
          </a>
          <a
            href="https://www.youtube.com/channel/UC1Dkp3hGbgyMlf4YhjvTg7w"
            target="_blank"
          >
            <img loading="lazy" src="/assets/social/youtube.png" />
          </a>
          <Link href="/">
            <img
              loading="lazy"
              src="/logo.png"
              alt="logo"
              style={{ marginRight: 0 }}
            />
          </Link>
        </SocialLayout>
      </HeaderLayout>
      <SideBar style={{ left: show ? "0px" : "-300px" }}>
        <CloseBtnLayout>
          <CloseBtn onClick={() => setShow(false)}>
            <i className="fa fa-times" />
          </CloseBtn>
        </CloseBtnLayout>

        <Link href="/lessons">
          <a onClick={() => setShow(false)}>
            <i className="fa fa-university" />
            Сургалтууд
          </a>
        </Link>
        <Link href="/schedule">
          <a onClick={() => setShow(false)}>
            <i className="fa fa-calendar-alt" />
            Хичээлийн хуваарь
          </a>
        </Link>
        <Link href="/about/0">
          <a onClick={() => setShow(false)}>
            <i className="fa fa-question-circle" />
            Яагаад гэгээ вэ?
          </a>
        </Link>
        <Link href="/gegeeTalks">
          <a onClick={() => setShow(false)}>
            <i className="fa fa-wallet" />
            Gegee Talks
          </a>
        </Link>
        <Link href="/advice">
          <a onClick={() => setShow(false)}>
            <i className="fa fa-comment-medical" />
            Зөвлөгөө
          </a>
        </Link>
        <Link href="/workplace">
          <a onClick={() => setShow(false)}>
            <i className="fa fa-briefcase" />
            Ажлын байр
          </a>
        </Link>
      </SideBar>
    </>
  );
};

export default MobileHeader;
