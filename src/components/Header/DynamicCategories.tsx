import Link from "next/link";
import { Dispatch } from "react";
import { ReactSVG } from "react-svg";
import { Categories } from "../../hooks/useCategories";

import { CardLayout } from "../Card";
import { MenuLayout, Menu } from "./Header";

const DynamicCatigories = ({
  show,
  setShow,
  categories,
}: {
  show: number;
  setShow: Dispatch<number>;
  categories: Categories;
}) => {
  return (
    <Link href="/lessons">
      <a style={{ color: show === 0 && "rgb(255, 166, 0)" }}>
        <span onMouseEnter={() => setShow(0)}>Сургалтууд</span>
        <div style={{ position: "relative", width: "100%" }}>
          <MenuLayout>
            <Menu style={{ display: show === 0 && "flex" }}>
              {categories?.map((cat, index) => (
                <Link
                  href={
                    cat.childrenCategories.length === 0
                      ? `/lessons/${cat._id}`
                      : `/lessons/child/${cat._id}`
                  }
                  key={index}
                >
                  <div
                    style={{
                      width: `${100 / (categories?.length + 1)}%`,
                      padding: "0px 6px",
                      borderRight:
                        categories?.length !== index &&
                        "1px solid rgb(230, 230, 230)",
                    }}
                  >
                    <CardLayout
                      style={{
                        border: "none",
                        width: `calc(100% - ${48}px)`,
                        padding: "8px 24px",
                        borderRadius: 10,
                      }}
                    >
                      <ReactSVG
                        src={`${process.env.API}${cat?.icon?.path}`}
                        beforeInjection={(svg) => {
                          svg.setAttribute(
                            "style",
                            "width: 50px; height: 50px;"
                          );
                        }}
                      />
                      {cat.name && (
                        <p
                          style={{
                            whiteSpace: "pre",
                            margin: "6px 0 0 0",
                            fontSize: 14,
                          }}
                        >
                          {cat.name}
                        </p>
                      )}
                    </CardLayout>
                  </div>
                </Link>
              ))}
              <Link href="/">
                <div
                  style={{
                    width: `${100 / (categories?.length + 1)}%`,
                    padding: "0 12px",
                  }}
                >
                  <CardLayout
                    style={{
                      border: "none",
                      padding: "0 20px 6px 20px",
                      borderRadius: 10,
                    }}
                  >
                    <ReactSVG
                      src="/assets/icons/level.svg"
                      beforeInjection={(svg) => {
                        svg.setAttribute(
                          "style",
                          "width: 50px; height: 50px; margin-top: 8px"
                        );
                      }}
                    />

                    <p style={{ whiteSpace: "pre", margin: "6px 0 0 0" }}>
                      Түвшин
                      <br /> тогтоох тест
                    </p>
                  </CardLayout>
                </div>
              </Link>
            </Menu>
          </MenuLayout>
        </div>
      </a>
    </Link>
  );
};

export default DynamicCatigories;
