import Link from "next/link";
import styled from "styled-components";

import { Dispatch } from "react";
import { Container } from "@material-ui/core";

import { Categories } from "../../hooks/useCategories";
import StaticCatigories from "./StaticCategories";
import DynamicCatigories from "./DynamicCategories";

const HeaderLayout = styled.div`
  background-color: rgba(255, 255, 255, 0.97);
  border-bottom: 1px solid rgb(230, 230, 230);
  width: 100%;
  padding: 20px 0 16px 0;
  position: fixed;
  top: 0;
  z-index: 3;
  img {
    cursor: pointer;
  }
  a {
    font-family: "Hermes";
    font-size: 15px;
    text-transform: uppercase;
    color: rgb(117, 117, 117);
    margin: 0 18px;
    cursor: pointer;
    text-decoration: none;
    &:first-child {
      margin-left: 0;
    }
    &:last-child {
      margin-right: 0;
    }
  }
  @media (max-width: 960px) {
    display: none;
  }
`;

export const Menu = styled.div`
  position: absolute;
  top: 36px;
  left: 0;
  right: auto;
  z-index: 10;
  display: none;
  align-items: center;
  padding: 6px 0px;
  border-radius: 10px;
  max-width: 1920px;
  background-color: #fff;
  box-shadow: 0 0.125rem 0.5rem rgba(0, 0, 0, 0.1),
    0 0.0625rem 0.125rem rgba(0, 0, 0, 0.1);

  &::before {
    content: "";
    position: absolute;
    width: 0;
    height: 0;
    bottom: 100%;
    left: 45px;
    transform: translate(-50%, 0);
    border: 10px solid transparent;
    border-top: none;
    border-bottom-color: #fff;
    filter: drop-shadow(0 -0.0625rem 0.0625rem rgba(0, 0, 0, 0.1));
  }
`;

export const FlexLayout = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
`;

export const MenuLayout = styled.div`
  position: absolute;
  background: transparent;
  height: 100px;
  width: 120%;
`;

const Header = ({
  show,
  setShow,
  categories,
}: {
  show: number;
  setShow: Dispatch<number>;
  categories: Categories;
}) => {
  return (
    <HeaderLayout>
      <Container maxWidth="md">
        <FlexLayout>
          <Link href="/">
            <img
              loading="lazy"
              src="/logo.png"
              alt="logo"
              style={{ width: 90 }}
            />
          </Link>
          <FlexLayout style={{ justifyContent: "flex-end" }}>
            <DynamicCatigories
              show={show}
              setShow={setShow}
              categories={categories}
            />
            <StaticCatigories show={show} setShow={setShow} />
          </FlexLayout>
        </FlexLayout>
      </Container>
    </HeaderLayout>
  );
};

export default Header;
