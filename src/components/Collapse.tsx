import { useState } from "react";
import styled from "styled-components";
import { Grid } from "@material-ui/core";

import { FlexLayout } from "./Header/Header";

export const CollapseItem = styled.div`
  color: rgb(117, 117, 117);
  border: 1px solid rgb(230, 230, 230);
  border-radius: 10px;
  padding: 18px 36px;
  cursor: pointer;

  .div {
    display: flex;
    align-items: flex-start;
  }
  strong {
    cursor: pointer;
    margin-right: 18px;
    font-weight: bold;
    color: ${(props) => props.color};
    text-transform: uppercase;
  }
  label {
    cursor: pointer;
    text-transform: uppercase;
  }
  i {
    font-size: 20px;
    cursor: pointer;
    transition: all 0.3s;
    transform: rotate(180deg);
  }
  @media (max-width: 900px) {
    font-size: 12px;
    padding: 8px 12px;
    strong {
      margin-right: 6px;
    }
    .div {
      display: block;
    }
  }
`;

export const CollapseBodyItem = styled.div`
  text-align: center;
  border-right: 1px solid ${(props) => props.color};
  padding: 0 6px;
  height: 100%;

  p {
    font-size: 14px;
    color: ${(props) => props.color};
    margin: 0;
    white-space: nowrap;
  }
  span {
    display: inherit;
    margin-top: 12px;
    font-size: 20px;
  }
  @media (max-width: 900px) {
    display: block;
    border-right: 0;
    padding: 0;
    span {
      font-size: 14px;
      margin-top: 6px;
    }
  }
`;

export const CollapseBody = styled.div`
  margin-top: 18px;
  padding: 12px 0;
  border-top: 1px solid rgba(230, 230, 230);

  label {
    text-transform: none;
    display: inherit;
    margin: 6px 0 36px 0;
    color: black;
  }

  .browser {
    display: block;
  }
  .mobile {
    display: none;
  }
  @media (max-width: 900px) {
    .browser {
      display: none;
    }
    .mobile {
      display: block;
    }
    label {
      text-transform: none;
      display: inherit;
      margin: 6px 0 24px 0;
      color: black;
    }
  }
`;

export type CollapseData = {
  title: string;
  value: any;
};

const Collapse = ({
  index,
  color,
  title,
  description,
  data,
}: {
  index: number;
  color: string;
  title?: string;
  description?: string;
  data: CollapseData[];
}) => {
  const [show, setShow] = useState(false);
  return (
    <CollapseItem onClick={() => setShow(!show)} color={color}>
      <FlexLayout style={{ alignItems: "center" }}>
        <div className="div">
          <strong>Түвшин {index}:</strong>
          <label>{title}</label>
        </div>
        <i
          className={"fal fa-angle-up"}
          style={{ transform: show ? "rotate(180deg)" : "rotate(0)" }}
        />
      </FlexLayout>
      <CollapseBody style={{ display: show ? "block" : "none" }}>
        <label>{description}</label>
        <div className="browser">
          <Grid container justify="flex-start">
            {data?.map((item, index) => (
              <div style={{ width: `${100 / data.length}%` }}>
                <CollapseBodyItem
                  color={color}
                  style={{ border: data.length - 1 === index && "none" }}
                >
                  <p>{item.title}:</p>
                  <span>{item.value}</span>
                </CollapseBodyItem>
              </div>
            ))}
          </Grid>
        </div>
        <div className="mobile">
          <Grid container spacing={3}>
            {data?.map((item, index) => (
              <Grid item xs={6}>
                <CollapseBodyItem color={color}>
                  <p>{item.title}:</p>
                  <span>{item.value}</span>
                </CollapseBodyItem>
              </Grid>
            ))}
          </Grid>
        </div>
      </CollapseBody>
    </CollapseItem>
  );
};

export default Collapse;
