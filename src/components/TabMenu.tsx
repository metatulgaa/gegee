import { Container } from "@material-ui/core";
import { ReactNode, useState } from "react";
import styled from "styled-components";

export const TabLayout = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  background-color: white;
  a {
    text-decoration: none;
    color: rgba(117, 117, 117, 0.6);
    padding: 12px 0;
    font-size: 20px;
    text-align: center;
    border-radius: 10px;
    font-family: "Hermes";
    cursor: pointer;
  }

  a.active {
    background-color: ${(props) => props?.color};
    color: white;
  }
  @media (max-width: 960px) {
    a {
      font-size: 16px;
      border-radius: 5px;
      font-weight: 400;
      width: 100% !important;
    }
    white-space: nowrap;
    width: auto;
    padding: 0 !important;
    flex-direction: column;
  }
`;

export type TabMenus = {
  index: number;
  name: string;
};

const TabMenu = ({
  menus,
  children,
  color,
  colors,
  selectKey,
}: {
  menus: TabMenus[];
  children: ReactNode[];
  color?: string;
  colors?: string[];
  selectKey: number;
}) => {
  const [show, setShow] = useState(selectKey);

  return (
    <div>
      <Container maxWidth="md">
        <TabLayout color={color}>
          {menus?.map((menu, index) => (
            <a
              className={show === menu.index && "active"}
              key={menu.index}
              onClick={() => setShow(menu.index)}
              style={{
                backgroundColor: colors && show === menu.index && colors[index],
                width: `calc(100% / ${menus.length})`,
              }}
            >
              {menu.name}
            </a>
          ))}
        </TabLayout>
      </Container>
      {children[show] ? children[show] : <div style={{ height: "100vh" }} />}
    </div>
  );
};

export default TabMenu;
