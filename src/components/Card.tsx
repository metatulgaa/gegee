import Link from "next/link";
import styled from "styled-components";
import { ReactSVG } from "react-svg";

export const CardLayout = styled.div`
  border-radius: 30px;
  position: relative;
  padding: 25px 20px;
  width: calc(100% - 40px);
  height: calc(100% - 50px);
  text-align: center;
  color: rgb(117, 117, 117);
  background-color: transparent;
  border: 1px solid rgb(230, 230, 230);
  transition: ease 0.5s;

  svg {
    width: 70px;
    height: 70px;
  }
  h1 {
    font-family: "Hermes";
    font-size: 19px;
    font-weight: 300;
    text-transform: uppercase;
    margin-bottom: 0;
    white-space: nowrap;
  }
  label {
    font-family: "Magistral";
    white-space: nowrap;
    font-size: 14px;
  }
  &:hover {
    cursor: pointer;
    border-color: rgb(255, 166, 0);
    background-color: rgb(255, 166, 0);
    color: white;
    svg .fil0 {
      fill: #fff !important;
    }
    svg .fil1 {
      fill: #fff !important;
    }
  }
  @media (max-width: 1200px) {
    h1 {
      font-size: 16px;
    }
    label {
      white-space: nowrap;
      font-size: 14px;
    }
  }
  @media (max-width: 960px) {
    padding: 12px;
    width: calc(100% - 24px);
    height: calc(100% - 24px);
    svg {
      width: 50px;
      height: 50px;
    }
    h1 {
      font-size: 14px;
      text-transform: uppercase;
    }
    label {
      font-size: 13px;
    }
  }
`;

const Card = ({
  imgPath,
  title,
  description,
  pathname,
  border,
}: {
  imgPath?: string;
  title?: string;
  description?: string;
  pathname?: string;
  border?: boolean;
}) => {
  return (
    <Link href={`${pathname}`}>
      <CardLayout style={{ border: !border && "none" }}>
        <ReactSVG src={imgPath} />
        {/* <img src={imgPath} /> */}

        {title && <h1>{title}</h1>}
        {description && <label>{description}</label>}
      </CardLayout>
    </Link>
  );
};

export default Card;
