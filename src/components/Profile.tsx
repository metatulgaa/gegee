import styled from "styled-components";

export const ProfileLayout = styled.div`
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  color: rgb(117, 117, 117);
  img {
    width: 150px;
    height: 150px;
    object-position: center;
    object-fit: cover;
    border: none;
    border-radius: 50%;
    background-color: rgb(230, 230, 230);
    border: 1px solid rgb(230, 230, 230);
  }
  p {
    margin-bottom: 6px;
    font-weight: bold;
    font-size: 20px;
  }
  label {
    text-transform: uppercase;
    font-size: 12px;
  }
`;

export const Profile = ({
  imgPath,
  title,
  description,
}: {
  imgPath?: string;
  title?: string;
  description?: string;
}) => {
  return (
    <ProfileLayout>
      <img loading="lazy" src={imgPath} alt="зураг" />
      <p>{title}</p>
      <label>{description}</label>
    </ProfileLayout>
  );
};
