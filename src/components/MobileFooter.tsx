import { Container, Grid } from "@material-ui/core";
import styled from "styled-components";

export const FooterLayout = styled.div`
  display: none;
  background-color: rgb(117, 117, 117);
  padding: 32px 0;
  div {
    display: flex;
    align-items: center;
  }
  a {
    color: white;
    font-family: "Magistral";
    text-decoration: none;
  }
  span {
    width: 1px;
    background-color: rgb(255, 166, 0);
    height: 25px;
  }
  img {
    height: 35px;
  }
  @media (max-width: 960px) {
    display: block;
  }
`;

const MobileFooter = () => {
  return (
    <FooterLayout>
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={5}>
            <img
              loading="lazy"
              src="/assets/school-logo.png"
              alt="footerLogo1"
            />
          </Grid>
          <Grid item xs={5}>
            <img
              loading="lazy"
              src="/assets/nomba-logo.png"
              alt="footerLogo1"
            />
          </Grid>
          <Grid item xs={5}>
            <img
              src="/assets/footer/phone.png"
              style={{ height: 16, marginRight: 6 }}
            />
            <a
              href="tel:+97677004567"
              style={{ marginRight: 16, fontSize: 12 }}
            >
              77004567
            </a>
          </Grid>
          <Grid item xs={5}>
            <img src="/assets/footer/email.png" style={{ height: 16 }} />
            <a
              href="mailto:info@gegee.edu.mn"
              style={{ marginLeft: 6, fontSize: 12 }}
            >
              info@gegee.edu.mn
            </a>
          </Grid>
        </Grid>
      </Container>
    </FooterLayout>
  );
};

export default MobileFooter;
