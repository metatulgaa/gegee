import Skeleton from "@material-ui/lab/Skeleton";
import { Grid } from "@material-ui/core";

import { WhitCardLayout } from "../WhiteCard";

const WhiteCardLoading = () => (
  <WhitCardLayout>
    <Skeleton variant="circle" width={150} height={150} />
    <div>
      <h1>
        <Skeleton variant="text" width={150} />
      </h1>
    </div>
    <Grid container spacing={1}>
      {[1, 2, 3].map((item) => (
        <Grid item xs={12} key={item}>
          <Skeleton variant="text" />
        </Grid>
      ))}
    </Grid>
  </WhitCardLayout>
);

export default WhiteCardLoading;
