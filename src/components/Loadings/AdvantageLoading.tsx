import Skeleton from "@material-ui/lab/Skeleton";

const AdvantageLoading = () => (
  <>
    <tr>
      {[1, 2, 3, 4, 5].map((load) => (
        <td key={load}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <Skeleton variant="circle" width={80} height={80} />
            <p>
              <Skeleton variant="text" width={100} />
            </p>
          </div>
        </td>
      ))}
    </tr>
    <tr>
      {[1, 2, 3, 4, 5].map((load) => (
        <td key={load}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <Skeleton variant="circle" width={80} height={80} />
            <p>
              <Skeleton variant="text" width={100} />
            </p>
          </div>
        </td>
      ))}
    </tr>
  </>
);

export default AdvantageLoading;
