import Skeleton from "@material-ui/lab/Skeleton";

import { ProfileLayout } from "../Profile";

const ProdileLoading = () => (
  <ProfileLayout>
    <Skeleton variant="circle" width={150} height={150} />
    <p>
      <Skeleton variant="text" width={150} />
    </p>
    <label>
      <Skeleton variant="text" width={100} />
    </label>
  </ProfileLayout>
);

export default ProdileLoading;
