import Skeleton from "@material-ui/lab/Skeleton";

import { FlexLayout } from "../Header/Header";
import { CollapseItem } from "../Collapse";

const CollapseLoading = () => {
  return (
    <CollapseItem>
      <FlexLayout style={{ cursor: "pointer" }}>
        <label>
          <Skeleton variant="text" width={200} />
        </label>
        <Skeleton variant="circle" width={25} height={25} />
      </FlexLayout>
    </CollapseItem>
  );
};

export default CollapseLoading;
