import { Grid } from "@material-ui/core";
import { Fragment } from "react";
import styled from "styled-components";
import { format } from "date-fns";

const ScheduleLayout = styled.div`
  background-color: white;
  border-bottom: 1px solid rgb(247, 247, 247);
  font-family: "Magistral";
  height: 100%;
`;

const ScheduleTitle = styled.div`
  width: 100%;
  display: flex;
  padding: 12px 0;
  align-items: center;
  justify-content: center;

  p {
    margin: 0;
    font-weight: bold;
    font-size: 20px;
    margin-right: 8px;
    color: rgb(255, 166, 0);
  }
  label {
    font-size: 20px;
    color: rgb(117, 117, 117);
  }
`;

const ScheduleHeader = styled.div`
  color: rgb(255, 166, 0);
  font-size: 18px;
  padding-top: 6px;
  margin-top: 12px;
  margin-bottom: 0;
`;

const ScheduleItem = styled.div`
  border-bottom: 3px solid rgb(247, 247, 247);

  padding: 6px 0 24px 0;
  p {
    margin: 6px 0;
    padding: 3px 0;
    color: rgb(117, 117, 117);
  }
  a {
    margin: 6px 0;
    width: 70px;
    padding: 3px;
    text-align: center;
    display: list-item;
    background-color: orange;
    color: white;
    border-radius: 3px;
  }
  &:last-child {
    border-bottom: none;
  }
`;

const Schedule = ({ data }: { data: any }) => {
  const color = ["rgb(255, 166, 0)", "rgb(255, 102, 102)", "#b960a7"];
  const borderColor = ["rgb(247, 247, 247)", "rgb(255, 102, 102)", "#b960a7"];

  return (
    <ScheduleLayout>
      <ScheduleTitle>
        <p>{format(new Date(data?.startDate), "YYYY-MM-DD")}</p>
        <label>эхлэх ангиуд</label>
      </ScheduleTitle>
      {data?.catLessons?.map((item, lessonIndex) => (
        <Fragment key={lessonIndex}>
          <div
            style={{
              borderTop: `3px solid ${borderColor[lessonIndex]}`,
            }}
          >
            <Grid container spacing={5}>
              <Grid item xs={5}>
                <ScheduleHeader
                  style={{
                    color: color[lessonIndex],
                    textAlign: "right",
                  }}
                >
                  {item?.category.name}
                </ScheduleHeader>
              </Grid>
              <Grid item xs={3}>
                <ScheduleHeader
                  style={{
                    color: color[lessonIndex],
                  }}
                >
                  Эхлэх цаг
                </ScheduleHeader>
              </Grid>
              <Grid item xs={4}>
                <ScheduleHeader style={{ color: color[lessonIndex] }}>
                  <span>Танхим/</span>
                  <span style={{ color: "#0099ff" }}>Онлайн</span>
                </ScheduleHeader>
              </Grid>
            </Grid>

            {item.schedule.map((level, levelIndex) => (
              <ScheduleItem key={levelIndex}>
                <Grid container spacing={5}>
                  <Grid item xs={5} style={{ textAlign: "right" }}>
                    <p>{level.lesson.name}</p>
                  </Grid>
                  <Grid item xs={3}>
                    {level?.times?.map((time, timeIndex) => (
                      <p key={timeIndex}>{time.startTime}</p>
                    ))}
                  </Grid>
                  <Grid item xs={4}>
                    {level?.times?.map((time, timeIndex) => (
                      <Fragment key={timeIndex}>
                        {time.type === "online" && (
                          <a style={{ backgroundColor: color[levelIndex] }}>
                            Онлайн
                          </a>
                        )}
                        {time.type === "hall" && (
                          <a style={{ backgroundColor: "#0099ff" }}>Танхим</a>
                        )}
                      </Fragment>
                    ))}
                  </Grid>
                </Grid>
              </ScheduleItem>
            ))}
          </div>
        </Fragment>
      ))}
    </ScheduleLayout>
  );
};

export default Schedule;
