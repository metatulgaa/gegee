import { Container } from "@material-ui/core";
import styled from "styled-components";

import { FlexLayout } from "./Header/Header";

export const FooterLayout = styled.div`
  width: 100%;
  padding: 30px 0;
  background-color: rgb(117, 117, 117);
  div {
    display: flex;
    align-items: center;
    height: 100%;
  }
  a {
    color: white;
    font-family: "Magistral";
    text-decoration: none;
  }
  span {
    width: 1px;
    background-color: rgb(255, 166, 0);
    height: 25px;
  }
  img {
    height: 40px;
  }
  @media (max-width: 960px) {
    display: none;
  }
`;

const Footer = () => {
  return (
    <FooterLayout>
      <Container maxWidth="md">
        <FlexLayout>
          <div>
            <img
              style={{ marginRight: 16 }}
              loading="lazy"
              src="/assets/school-logo.png"
              alt="footerLogo1"
            />
            <span style={{ height: 40 }} />
            <img
              style={{ marginLeft: 16 }}
              loading="lazy"
              src="/assets/nomba-logo.png"
              alt="footerLogo1"
            />
          </div>
          <div>
            <img
              src="/assets/footer/phone.png"
              style={{ height: 20, marginRight: 6 }}
            />
            <a href="tel:+97677004567" style={{ marginRight: 16 }}>
              77004567
            </a>
            <span />
            <img
              src="/assets/footer/email.png"
              style={{ height: 20, marginLeft: 16 }}
            />
            <a href="mailto:info@gegee.edu.mn" style={{ marginLeft: 6 }}>
              info@gegee.edu.mn
            </a>
          </div>
        </FlexLayout>
      </Container>
    </FooterLayout>
  );
};

export default Footer;
