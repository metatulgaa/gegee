import styled from "styled-components";
import { Grid } from "@material-ui/core";
import { ReactSVG } from "react-svg";
import MarkUp from "./MarkUp";

const PostLayout = styled.div`
  width: 100%;
  position: relative;
  height: 100%;
  cursor: pointer;
  z-index: 0;
  .active {
    position: absolute;
    left: -12px;
    top: -12px;
    height: calc(100% + 24px);
    width: calc(100% + 24px);
    border: 1px solid rgb(255, 166, 0);
    border-radius: 3px;
    display: none;
    z-index: 0;
  }

  &:hover .active {
    display: block;
  }
`;

const PostImage = styled.div`
  width: 100%;
  height: 100%;
  background-color: #dedede;
  border-radius: 3px;
  background-size: cover;
  background-position: center;
  background-image: url(${(props) => props?.imgPath});
  @media (max-width: 960px) {
    height: 300px;
  }
`;

const PostDesc = styled.div`
  padding: 12px 36px;
  color: rgb(117, 117, 117);
  display: flex;
  height: calc(100% - 12px);
  flex-direction: column;
  justify-content: space-between;
  position: relative;
  .div {
    display: flex;
    align-items: center;
  }
  h1 {
    font-weight: 100;
    margin: 12px 0;
  }
  h3 {
    color: rgb(255, 166, 0);
    margin: 0;
    font-weight: 100;
    font-size: 20px;
  }
  label {
    display: block;
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  a {
    padding: 12px;
    display: flex;
    align-items: center;
    &:first-child {
      padding-left: 0;
    }
  }
  svg {
    margin-right: 6px;
    width: 20px;
    height: 20px;
  }
  @media (max-width: 960px) {
    padding: 24px 0;
    display: block;
    h1 {
      font-size: 22px;
    }
    h3 {
      font-size: 16px;
    }
    label {
      font-size: 14px;
    }
    .div {
      margin-top: 12px;
    }
  }
`;

export const Post = ({
  no,
  title,
  commendNo,
  date,
  imgPath,
  description,
}: {
  no: string;
  title: string;
  commendNo: number;
  date: string;
  imgPath?: string;
  description?: string;
}) => {
  return (
    <PostLayout>
      <span className="active" />
      <Grid container>
        <Grid item xs={12} sm={3}>
          <PostImage imgPath={imgPath} />
        </Grid>
        <Grid item xs={12} sm={9}>
          <PostDesc>
            <div>
              <h3>{no}</h3>
              <h1>{title}</h1>
              <label>
                <div
                  className="editer_style"
                  dangerouslySetInnerHTML={MarkUp(description)}
                />
              </label>
            </div>
            <div className="div">
              <a>
                <ReactSVG src="/assets/icons/comment.svg" />
                {commendNo}
              </a>
              <a>
                <ReactSVG src="/assets/icons/clock.svg" />
                {date}
              </a>
            </div>
          </PostDesc>
        </Grid>
      </Grid>
    </PostLayout>
  );
};
