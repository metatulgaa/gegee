import styled from "styled-components";
import { ReactNode, useState } from "react";

import { FlexLayout } from "./Header/Header";

const CollapseItem = styled.div`
  border: 1px solid rgb(230, 230, 230);
  border-radius: 10px;
  padding: 18px 36px;
  color: rgb(117, 117, 117);
  z-index: 0;

  div {
    display: flex;
    align-items: center;
  }

  i {
    font-size: 20px;
    cursor: pointer;
    transition: all 0.3s;
    transform: rotate(180deg);
  }
`;

const CollapseBody = styled.div`
  margin-top: 16px;
  padding: 12px 0;
  border-top: 1px solid ${(props) => props?.color};

  label {
    text-transform: none;
    display: inherit;
    margin-bottom: 24px;
    color: black;
  }
`;

export type CollapseData = {
  title: string;
  value: string;
};

const ChildCollapse = ({
  color,
  title,
  children,
}: {
  color: string;
  title?: string;
  children: ReactNode;
}) => {
  const [show, setShow] = useState(false);
  return (
    <CollapseItem color={color}>
      <FlexLayout onClick={() => setShow(!show)} style={{ cursor: "pointer" }}>
        <label style={{ color: show && color }}>{title}</label>
        <i
          className={"fal fa-angle-up"}
          style={{ transform: show ? "rotate(180deg)" : "rotate(0)" }}
        />
      </FlexLayout>
      <CollapseBody color={color} style={{ display: show ? "block" : "none" }}>
        {children}
      </CollapseBody>
    </CollapseItem>
  );
};

export default ChildCollapse;
