import styled from "styled-components";

export const SocialLayout = styled.div`
  position: absolute;
  z-index: 2;
  bottom: 15px;
  right: 0;
  background-color: white;
  padding: 8px 6px;
  display: flex;
  flex-direction: column;
  border-radius: 5px 0 0 5px;
  ::before {
    content: "";
    position: absolute;
    width: 0;
    height: 0;
    top: 100%;
    left: 35px;
    transform: translate(-50%, 0);
    border: 15px solid transparent;
    border-bottom: none;
    border-right: none;
    border-top-color: #fff;
  }
  img {
    width: 25px;
    margin: 3px 0;
    cursor: pointer;
    opacity: 1;
    transition: opacity 0.5s;
  }
  img:hover {
    opacity: 0.6;
  }
  @media (max-width: 960px) {
    display: none;
  }
`;

const Social = ({ bottom }: { bottom?: string }) => {
  return (
    <SocialLayout style={{ bottom: bottom ? bottom : "15px" }}>
      <a href="https://www.facebook.com/GEGEEteam/" target="_blank">
        <img loading="lazy" src="/assets/social/facebook.png" />
      </a>
      <a href="https://www.instagram.com/gegee.learnsmarter/" target="_blank">
        <img loading="lazy" src="/assets/social/instagram.png" />
      </a>
      <a
        href="https://www.youtube.com/channel/UC1Dkp3hGbgyMlf4YhjvTg7w"
        target="_blank"
      >
        <img loading="lazy" src="/assets/social/youtube.png" />
      </a>
    </SocialLayout>
  );
};

export default Social;
