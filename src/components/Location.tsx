import styled from "styled-components";

export const Location = styled.div`
  i {
    font-size: 20px;
    margin-right: 12px;
    background-color: orange;
    color: white;
    padding: 6px;
    border-radius: 3px;
  }
  p {
    color: rgb(117, 117, 117);
    display: flex;
    align-items: center;
    font-weight: bold;
  }
`;
