import { Container } from "@material-ui/core";
import Link from "next/link";
import styled from "styled-components";

export const BreadcumbLayout = styled.div`
  width: 100%;
  background-color: rgb(230, 230, 230);
  padding: 12px 0;
  div {
    display: flex;
    align-items: center;
  }
  a {
    font-family: "Magistral";
    color: rgb(117, 117, 117);
    text-decoration: none;
    display: flex;
    align-items: center;
    margin: 0;
    transition: all 0.3s;
    &:hover {
      color: orange;
    }
  }
  i {
    font-size: 22px;
    margin: 0 12px;
    color: rgba(117, 117, 117, 0.6);
  }
  @media (max-width: 960px) {
    font-size: 12px;
    white-space: nowrap;
    overflow-x: auto;
    i {
      font-size: 12px;
    }
  }
`;

export type BreadcumbCats = {
  name: string;
  index: number;
  pathname: string;
};

const Breadcumb = ({ categories }: { categories: BreadcumbCats[] }) => {
  return (
    <BreadcumbLayout>
      <Container maxWidth="md">
        <div>
          {categories?.map((item) => (
            <Link href={item.pathname} key={item.index}>
              <a>
                {item.name}
                {item.index !== categories.length && (
                  <i className="fal fa-angle-right" />
                )}
              </a>
            </Link>
          ))}
        </div>
      </Container>
    </BreadcumbLayout>
  );
};

export default Breadcumb;
