import { Grid } from "@material-ui/core";
import styled from "styled-components";
import { AnswerResult, Result } from "../../widgets/test";

export const QuestionItem = styled.div`
  p {
    margin: 0 0 24px 0;
    font-size: 18px;
    font-weight: 400;
  }
  h1 {
    margin: 0 0 36px 0;
    font-weight: 400;
    line-height: 25px;
    font-size: 25px;
    color: rgb(117, 117, 117);
  }
  img {
    position: absolute;
    width: 350px;
    object-fit: contain;
  }
  button {
    border: 1px solid #000;
    background-color: transparent;
    color: rgb(117, 117, 117);
    border: 1px solid rgb(117, 117, 117);
    padding: 8px 12px;
    border-radius: 10px;
    transition: all 0.5s;
    cursor: pointer;
    outline: none;
    margin: 0 12px 0 0;
  }

  @media (max-width: 1200px) {
  }
  @media (max-width: 960px) {
  }
`;

const FlexLayout = styled.div`
  display: flex;
  align-items: center;
  h1 {
    margin: 0 16px 0 0;
  }
`;

const OneQuestionManyAnswer = ({
  name,
  question,
  test,
  imgPath,
  result,
  handleChange,
  answerResult,
}: {
  name: string;
  question: string;
  test: [];
  imgPath: string;
  result: Result[];
  handleChange: (value: AnswerResult) => void;
  answerResult: AnswerResult[];
}) => {
  return (
    <QuestionItem>
      <Grid container justify="flex-end" spacing={2}>
        <Grid item xs={12} sm={2}>
          <p>{name}</p>
        </Grid>
        <Grid item xs={12} sm={10}>
          <p>Тооны оронд тохирох хариултуудыг сонгоно уу.</p>
        </Grid>
        <Grid item xs={12} sm={8}>
          <h1>{question}</h1>
          <Grid container spacing={2}>
            {test.map((answer: any) => (
              <Grid item xs={12}>
                <FlexLayout
                  style={{ alignItems: "center", justifyConten: "flex-start" }}
                >
                  <h1>{answer.id + 1}. </h1>
                  {answer.answers.map((item: any) => (
                    <button
                      disabled={
                        answerResult?.find(
                          (f) => Number(f.testId) === Number(answer.id)
                        ) && true
                      }
                      className={
                        answerResult?.find(
                          (f) =>
                            Number(f.testId) === Number(answer.id) &&
                            Number(f.answerKey) === Number(item.key)
                        ) && "clicked"
                      }
                      style={{
                        color:
                          result?.find((res) => res.testId === answer.id)
                            ?.trueKey === item.key && "white",
                        backgroundColor:
                          result?.find((res) => res.testId === answer.id)
                            ?.trueKey === item.key && "rgb(153, 204, 51) ",
                        borderColor:
                          result?.find((res) => res.testId === answer.id)
                            ?.trueKey === item.key && "rgb(153, 204, 51) ",
                      }}
                      onClick={() =>
                        handleChange({
                          testId: answer?.id,
                          answerKey: item.key,
                        })
                      }
                    >
                      {item.answer}
                    </button>
                  ))}
                </FlexLayout>
              </Grid>
            ))}
          </Grid>
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src={imgPath} alt="img" />
        </Grid>
      </Grid>
    </QuestionItem>
  );
};

export default OneQuestionManyAnswer;
