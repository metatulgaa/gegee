import { Grid } from "@material-ui/core";
import { useState } from "react";
import { useEffect } from "react";
import { Dispatch } from "react";
import styled from "styled-components";
import { AnswerResult, Result } from "../../widgets/test";

export const QuestionItem = styled.div`
  p {
    margin: 0;
    font-size: 18px;
    font-weight: 400;
  }
  h1 {
    margin: 0 0 12px 0;
    color: rgb(117, 117, 117);
    font-weight: 400;
    font-size: 25px;
  }
  button {
    color: rgb(117, 117, 117);
    border: 1px solid rgb(117, 117, 117);
    background-color: transparent;
    padding: 8px 0;
    width: 100%;
    border-radius: 10px;
    transition: all 0.5s;
    cursor: pointer;
    outline: none;
    margin: 0;
  }

  @media (max-width: 1200px) {
  }
  @media (max-width: 960px) {
  }
`;

const QuestionAnswer = ({
  name,
  test,
  result,
  answerResult,
  handleChange,
}: {
  name: string;
  test: [];
  result: Result[];
  answerResult: AnswerResult[];
  handleChange: (value: AnswerResult) => void;
}) => {
  return (
    <QuestionItem>
      {name && test && (
        <Grid container justify="flex-end" spacing={3}>
          <Grid item xs={12} sm={2}>
            <p>{name}</p>
          </Grid>
          <Grid item xs={12} sm={10}>
            <p>Зөв хариултыг нь сонгоно уу.</p>
          </Grid>
          {test?.map((item: any) => (
            <Grid item xs={12} sm={10} key={item.id}>
              <h1>
                {Number(item.id) + 1}. {item?.question}
              </h1>
              <Grid container spacing={2}>
                {item.answers.map((ans) => (
                  <Grid item xs={6} sm={3}>
                    <button
                      key={ans.key}
                      disabled={
                        answerResult?.find(
                          (f) => Number(f.testId) === Number(item.id)
                        ) && true
                      }
                      className={
                        answerResult?.find(
                          (f) =>
                            Number(f.testId) === Number(item.id) &&
                            Number(f.answerKey) === Number(ans.key)
                        ) && "clicked"
                      }
                      style={{
                        color:
                          result?.find((result) => result.testId === item.id)
                            ?.trueKey === ans.key && "white",
                        backgroundColor:
                          result?.find((result) => result.testId === item.id)
                            ?.trueKey === ans.key && "rgb(153, 204, 51) ",
                        borderColor:
                          result?.find((result) => result.testId === item.id)
                            ?.trueKey === ans.key && "rgb(153, 204, 51) ",
                      }}
                      onClick={() =>
                        handleChange({ testId: item.id, answerKey: ans.key })
                      }
                    >
                      {ans.answer}
                    </button>
                  </Grid>
                ))}
              </Grid>
            </Grid>
          ))}
        </Grid>
      )}
    </QuestionItem>
  );
};

export default QuestionAnswer;
