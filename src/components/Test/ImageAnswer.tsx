import { Grid } from "@material-ui/core";
import styled from "styled-components";
import { AnswerResult, Result } from "../../widgets/test";

export const QuestionItem = styled.div`
  p {
    margin: 0 0 24px 0;
    font-size: 18px;
    font-weight: 400;
  }
  h1 {
    margin: 0 0 36px 0;
    font-weight: 400;
    line-height: 25px;
    font-size: 25px;
    color: rgb(117, 117, 117);
  }

  button {
    border: 1px solid #000;
    background-color: transparent;
    color: rgb(117, 117, 117);
    border: 1px solid rgb(117, 117, 117);
    padding: 8px 16px;
    border-radius: 10px;
    transition: all 0.5s;
    cursor: pointer;
    outline: none;
    margin: 0 12px 12px 0;
  }

  @media (max-width: 1200px) {
  }
  @media (max-width: 960px) {
  }
`;

const FlexLayout = styled.div`
  display: flex;
  align-items: center;
  padding: 0 0 12px 0;
  h1 {
    margin: 0 16px 0 0;
  }
  img {
    width: 200px;
    object-fit: contain;
    margin-right: 24px;
  }
`;

const ButtonLayout = styled.div`
  display: flex;
  justify-content: flex-start;
`;

const ImageAnswer = ({
  name,
  test,
  result,
  answerResult,
  handleChange,
}: {
  name: string;
  test: [];
  result: Result[];
  answerResult: AnswerResult[];
  handleChange: (value: AnswerResult) => void;
}) => {
  return (
    <QuestionItem>
      <Grid container justify="flex-end" spacing={3}>
        <Grid item xs={12} sm={2}>
          <p>{name}</p>
        </Grid>
        <Grid item xs={12} sm={10}>
          <p>Тооны оронд тохирох хариултуудыг сонгоно уу.</p>
        </Grid>
        {test.map((item: any) => (
          <Grid item xs={12} sm={10}>
            <FlexLayout>
              <h1>{item.id + 1}.</h1>
              <img src={item.imgPath} />
              <div>
                <ButtonLayout>
                  {item.answers.slice(0, 2).map((ans) => (
                    <button
                      disabled={
                        answerResult?.find(
                          (f) => Number(f.testId) === Number(item.id)
                        ) && true
                      }
                      className={
                        answerResult?.find(
                          (f) =>
                            Number(f.testId) === Number(item.id) &&
                            Number(f.answerKey) === Number(ans.key)
                        ) && "clicked"
                      }
                      style={{
                        color:
                          result?.find(
                            (f) => Number(f.testId) === Number(item.id)
                          )?.trueKey === ans.key && "white",
                        backgroundColor:
                          result?.find(
                            (f) => Number(f.testId) === Number(item.id)
                          )?.trueKey === ans.key && "rgb(153, 204, 51)",
                        borderColor:
                          result?.find(
                            (f) => Number(f.testId) === Number(item.id)
                          )?.trueKey === ans.key && "rgb(153, 204, 51)",
                      }}
                      onClick={() =>
                        handleChange({ testId: item.id, answerKey: ans.key })
                      }
                    >
                      {ans.answer}
                    </button>
                  ))}
                </ButtonLayout>
                <ButtonLayout>
                  {item.answers.slice(2, 4).map((ans) => (
                    <button
                      disabled={
                        answerResult?.find(
                          (f) => Number(f.testId) === Number(item.id)
                        ) && true
                      }
                      style={{
                        color:
                          result?.find((res) => res.testId === item.id)
                            .trueKey === ans.key && "white",
                        backgroundColor:
                          result?.find((res) => res.testId === item.id)
                            .trueKey === ans.key && "rgb(153, 204, 51) ",
                        borderColor:
                          result?.find((res) => res.testId === item.id)
                            .trueKey === ans.key && "rgb(153, 204, 51) ",
                      }}
                      className={
                        answerResult?.find(
                          (f) =>
                            Number(f.testId) === Number(item.id) &&
                            Number(f.answerKey) === Number(ans.key)
                        ) && "clicked"
                      }
                      onClick={() =>
                        handleChange({ testId: item.id, answerKey: ans.key })
                      }
                    >
                      {ans.answer}
                    </button>
                  ))}
                </ButtonLayout>
              </div>
            </FlexLayout>
          </Grid>
        ))}
      </Grid>
    </QuestionItem>
  );
};

export default ImageAnswer;
