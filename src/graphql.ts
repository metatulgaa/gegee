import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type AddAdvantageInput = {
  name: Scalars['String'];
  image: AddImageInput;
};

export type AddArticleInput = {
  title: Scalars['String'];
  description: Scalars['String'];
  body: Scalars['String'];
  image: AddImageInput;
  type: ArticleType;
};

export type AddCategoryInput = {
  name: Scalars['String'];
  tag: Scalars['String'];
  image: AddImageInput;
  icon: AddImageInput;
  color: Scalars['String'];
  parentId?: Maybe<Scalars['ID']>;
};

export type AddCommentInput = {
  author: Scalars['String'];
  body: Scalars['String'];
};

export type AddEmployeeInput = {
  name: Scalars['String'];
  bio: Scalars['String'];
  image: AddImageInput;
  highlight: Scalars['Boolean'];
  positionId: Scalars['ID'];
};

export type AddGalleryInput = {
  name: Scalars['String'];
  images?: Maybe<Array<GalleryImageInput>>;
  category: Scalars['String'];
};

export type AddImageInput = {
  path: Scalars['String'];
};

export type AddLessonInput = {
  index: Scalars['Int'];
  name: Scalars['String'];
  description: Scalars['String'];
  level: Scalars['String'];
  price: Scalars['Float'];
  period: Scalars['Float'];
  days?: Maybe<Array<Scalars['String']>>;
  studyTime: Scalars['Int'];
  studentLimit: Scalars['String'];
  categoryId: Scalars['ID'];
};

export type AddPositionInput = {
  name: Scalars['String'];
};

export type AddReasonInput = {
  name: Scalars['String'];
  body: Scalars['String'];
  image: AddImageInput;
};

export type AddScheduleInput = {
  startDate: Scalars['Float'];
  lesson: Scalars['ID'];
  times: Array<AddTimeInput>;
  category: Scalars['ID'];
};

export type AddTimeInput = {
  startTime: Scalars['String'];
  type: Scalars['String'];
};

export type AddUserInput = {
  username: Scalars['String'];
  firstname?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  password: Scalars['String'];
  position?: Maybe<Scalars['ID']>;
};

export type AddWorkplaceInput = {
  name: Scalars['String'];
  goal?: Maybe<Scalars['String']>;
  generalRequirements?: Maybe<Array<Scalars['String']>>;
  requirements?: Maybe<Array<Scalars['String']>>;
  expiryDate: Scalars['Float'];
};

export type Advantage = {
  __typename?: 'Advantage';
  _id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  image: Image;
  updatedBy?: Maybe<User>;
  createdBy: User;
  updatedAt?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
};

export type Article = {
  __typename?: 'Article';
  _id?: Maybe<Scalars['ID']>;
  title: Scalars['String'];
  description: Scalars['String'];
  body: Scalars['String'];
  image: Image;
  type: ArticleType;
  commentsCount: Scalars['Int'];
  updatedBy?: Maybe<User>;
  createdBy: User;
  updatedAt?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
};

export type ArticleListResult = {
  __typename?: 'ArticleListResult';
  articles?: Maybe<Array<Article>>;
  totalPages: Scalars['Int'];
};

export enum ArticleType {
  Talk = 'TALK',
  Advice = 'ADVICE'
}

export type Auth = {
  __typename?: 'Auth';
  token: Scalars['String'];
  user: User;
};

export type Category = {
  __typename?: 'Category';
  _id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  tag: Scalars['String'];
  image: Image;
  icon: Image;
  color: Scalars['String'];
  parentCategory?: Maybe<Category>;
  childrenCategories?: Maybe<Array<Category>>;
  updatedBy?: Maybe<User>;
  createdBy: User;
  updatedAt?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
};

export type Comment = {
  __typename?: 'Comment';
  _id?: Maybe<Scalars['ID']>;
  author: Scalars['String'];
  body: Scalars['String'];
  parentComment?: Maybe<Comment>;
  article: Article;
  replies?: Maybe<Array<Comment>>;
};

export type CommentListResult = {
  __typename?: 'CommentListResult';
  comments?: Maybe<Array<Comment>>;
  totalPages: Scalars['Int'];
};

export type EditAdvantageInput = {
  name: Scalars['String'];
  image: EditImageInput;
};

export type EditArticleInput = {
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  body?: Maybe<Scalars['String']>;
  image: EditImageInput;
  type?: Maybe<ArticleType>;
};

export type EditCategoryInput = {
  name?: Maybe<Scalars['String']>;
  image: EditImageInput;
  icon: EditImageInput;
  color?: Maybe<Scalars['String']>;
  tag?: Maybe<Scalars['String']>;
};

export type EditEmployeeInput = {
  name?: Maybe<Scalars['String']>;
  bio?: Maybe<Scalars['String']>;
  image: EditImageInput;
  highlight?: Maybe<Scalars['Boolean']>;
  positionId?: Maybe<Scalars['ID']>;
};

export type EditGalleryInput = {
  name?: Maybe<Scalars['String']>;
  images?: Maybe<Array<GalleryImageInput>>;
  category?: Maybe<Scalars['String']>;
};

export type EditImageInput = {
  path?: Maybe<Scalars['String']>;
};

export type EditLessonInput = {
  index?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  period?: Maybe<Scalars['Float']>;
  days?: Maybe<Array<Scalars['String']>>;
  studyTime?: Maybe<Scalars['Int']>;
  studentLimit?: Maybe<Scalars['String']>;
  categoryId?: Maybe<Scalars['ID']>;
};

export type EditReasonInput = {
  name?: Maybe<Scalars['String']>;
  body?: Maybe<Scalars['String']>;
  image: EditImageInput;
};

export type EditScheduleInput = {
  startDate?: Maybe<Scalars['Float']>;
  lesson?: Maybe<Scalars['ID']>;
  times?: Maybe<Array<AddTimeInput>>;
  category?: Maybe<Scalars['ID']>;
};

export type EditUserInput = {
  username?: Maybe<Scalars['String']>;
  firstname?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['ID']>;
};

export type EditWorkplaceInput = {
  name?: Maybe<Scalars['String']>;
  goal?: Maybe<Scalars['String']>;
  generalRequirements?: Maybe<Array<Scalars['String']>>;
  requirements?: Maybe<Array<Scalars['String']>>;
  expiryDate?: Maybe<Scalars['Float']>;
};

export type Employee = {
  __typename?: 'Employee';
  _id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  bio: Scalars['String'];
  image: Image;
  highlight: Scalars['Boolean'];
  position?: Maybe<Position>;
  updatedBy?: Maybe<User>;
  createdBy: User;
  updatedAt?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
};

export type EmployeeListResult = {
  __typename?: 'EmployeeListResult';
  employees?: Maybe<Array<Employee>>;
  totalPages: Scalars['Int'];
};

export type Gallery = {
  __typename?: 'Gallery';
  _id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  images?: Maybe<Array<Image>>;
  category: Scalars['String'];
};

export type GalleryImageInput = {
  path: Scalars['String'];
};

export type Image = {
  __typename?: 'Image';
  _id?: Maybe<Scalars['ID']>;
  path: Scalars['String'];
};

export type ImageListResult = {
  __typename?: 'ImageListResult';
  images?: Maybe<Array<Image>>;
  totalPages: Scalars['Int'];
};

export type Lesson = {
  __typename?: 'Lesson';
  _id?: Maybe<Scalars['ID']>;
  index: Scalars['Int'];
  name: Scalars['String'];
  description: Scalars['String'];
  level: Scalars['String'];
  price: Scalars['Float'];
  period: Scalars['Float'];
  days?: Maybe<Array<Scalars['String']>>;
  studyTime: Scalars['Int'];
  studentLimit: Scalars['String'];
  category: Category;
  updatedBy?: Maybe<User>;
  createdBy: User;
  updatedAt?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
};

export type LessonListResult = {
  __typename?: 'LessonListResult';
  lessons?: Maybe<Array<Lesson>>;
  totalPages: Scalars['Int'];
};

export type Mutation = {
  __typename?: 'Mutation';
  addAdvantage: Scalars['Boolean'];
  editAdvantage: Scalars['Boolean'];
  deleteAdvantage: Scalars['Boolean'];
  addArticle: Scalars['Boolean'];
  editArticle: Scalars['Boolean'];
  deleteArticle: Scalars['Boolean'];
  /** This is no-op mutation */
  _?: Maybe<Scalars['Boolean']>;
  addCategory: Scalars['Boolean'];
  editCategory: Scalars['Boolean'];
  deleteCategory: Scalars['Boolean'];
  addComment: Scalars['Boolean'];
  deleteComment: Scalars['Boolean'];
  addEmployee: Scalars['Boolean'];
  editEmployee: Scalars['Boolean'];
  deleteEmployee: Scalars['Boolean'];
  addGallery: Scalars['Boolean'];
  editGallery: Scalars['Boolean'];
  deleteGallery: Scalars['Boolean'];
  addImage: Scalars['Boolean'];
  deleteImage: Scalars['Boolean'];
  addLesson: Scalars['Boolean'];
  editLesson: Scalars['Boolean'];
  deleteLesson: Scalars['Boolean'];
  addPosition: Scalars['Boolean'];
  editPosition: Scalars['Boolean'];
  deletePosition: Scalars['Boolean'];
  addReason: Scalars['Boolean'];
  editReason: Scalars['Boolean'];
  deleteReason: Scalars['Boolean'];
  register: Scalars['Boolean'];
  deleteRegistration: Scalars['Boolean'];
  addSchedule: Scalars['Boolean'];
  editSchedule: Scalars['Boolean'];
  deleteSchedule: Scalars['Boolean'];
  login: Auth;
  addUser: Scalars['Boolean'];
  editUser: Scalars['Boolean'];
  deleteUser: Scalars['Boolean'];
  addWorkplace: Scalars['Boolean'];
  editWorkplace: Scalars['Boolean'];
  deleteWorkplace: Scalars['Boolean'];
};


export type MutationAddAdvantageArgs = {
  advantage: AddAdvantageInput;
};


export type MutationEditAdvantageArgs = {
  _id: Scalars['ID'];
  advantage: EditAdvantageInput;
};


export type MutationDeleteAdvantageArgs = {
  _id: Scalars['ID'];
};


export type MutationAddArticleArgs = {
  article: AddArticleInput;
};


export type MutationEditArticleArgs = {
  _id: Scalars['ID'];
  article: EditArticleInput;
};


export type MutationDeleteArticleArgs = {
  _id: Scalars['ID'];
};


export type MutationAddCategoryArgs = {
  category: AddCategoryInput;
};


export type MutationEditCategoryArgs = {
  _id: Scalars['ID'];
  category: EditCategoryInput;
};


export type MutationDeleteCategoryArgs = {
  _id: Scalars['ID'];
};


export type MutationAddCommentArgs = {
  articleId: Scalars['ID'];
  commentId?: Maybe<Scalars['ID']>;
  comment: AddCommentInput;
};


export type MutationDeleteCommentArgs = {
  _id: Scalars['ID'];
};


export type MutationAddEmployeeArgs = {
  employee: AddEmployeeInput;
};


export type MutationEditEmployeeArgs = {
  _id: Scalars['ID'];
  employee: EditEmployeeInput;
};


export type MutationDeleteEmployeeArgs = {
  _id: Scalars['ID'];
};


export type MutationAddGalleryArgs = {
  gallery: AddGalleryInput;
};


export type MutationEditGalleryArgs = {
  _id: Scalars['ID'];
  gallery: EditGalleryInput;
};


export type MutationDeleteGalleryArgs = {
  _id: Scalars['ID'];
};


export type MutationAddImageArgs = {
  image: AddImageInput;
};


export type MutationDeleteImageArgs = {
  _id: Scalars['ID'];
};


export type MutationAddLessonArgs = {
  lesson: AddLessonInput;
};


export type MutationEditLessonArgs = {
  _id: Scalars['ID'];
  lesson: EditLessonInput;
};


export type MutationDeleteLessonArgs = {
  _id: Scalars['ID'];
};


export type MutationAddPositionArgs = {
  position: AddPositionInput;
};


export type MutationEditPositionArgs = {
  _id: Scalars['ID'];
  position: AddPositionInput;
};


export type MutationDeletePositionArgs = {
  _id: Scalars['ID'];
};


export type MutationAddReasonArgs = {
  reason: AddReasonInput;
};


export type MutationEditReasonArgs = {
  _id: Scalars['ID'];
  reason: EditReasonInput;
};


export type MutationDeleteReasonArgs = {
  _id: Scalars['ID'];
};


export type MutationRegisterArgs = {
  inputs: RegisterInput;
};


export type MutationDeleteRegistrationArgs = {
  _id: Scalars['ID'];
};


export type MutationAddScheduleArgs = {
  schedule: AddScheduleInput;
};


export type MutationEditScheduleArgs = {
  _id: Scalars['ID'];
  schedule: EditScheduleInput;
};


export type MutationDeleteScheduleArgs = {
  _id: Scalars['ID'];
};


export type MutationLoginArgs = {
  username: Scalars['String'];
  password: Scalars['String'];
};


export type MutationAddUserArgs = {
  user: AddUserInput;
};


export type MutationEditUserArgs = {
  _id: Scalars['ID'];
  user: EditUserInput;
};


export type MutationDeleteUserArgs = {
  _id: Scalars['ID'];
};


export type MutationAddWorkplaceArgs = {
  workplace: AddWorkplaceInput;
};


export type MutationEditWorkplaceArgs = {
  _id: Scalars['ID'];
  workplace: EditWorkplaceInput;
};


export type MutationDeleteWorkplaceArgs = {
  _id: Scalars['ID'];
};

export type Position = {
  __typename?: 'Position';
  _id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  updatedBy?: Maybe<User>;
  createdBy: User;
  updatedAt?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
};

export type Query = {
  __typename?: 'Query';
  getAdvantages?: Maybe<Array<Advantage>>;
  getAdvantage: Advantage;
  getArticles: ArticleListResult;
  getArticle: Article;
  getArticlesByType?: Maybe<Array<Article>>;
  /** Get current app version */
  version: Scalars['String'];
  getCategories?: Maybe<Array<Category>>;
  getCategory: Category;
  getComments?: Maybe<Array<Comment>>;
  getCommentsWithSearch: CommentListResult;
  getEmployees: EmployeeListResult;
  getEmployee: Employee;
  getAllEmployees?: Maybe<Array<Employee>>;
  getGalleries?: Maybe<Array<Gallery>>;
  getGallery: Gallery;
  getImages: ImageListResult;
  getLessons?: Maybe<Array<Lesson>>;
  getLessonsWithPage: LessonListResult;
  getLesson: Lesson;
  getPositions?: Maybe<Array<Position>>;
  getReasons?: Maybe<Array<Reason>>;
  getReason: Reason;
  getRegistrations: RegistrationListResult;
  getRegistration: Registration;
  getSchedules?: Maybe<Array<Schedule>>;
  getSchedulesWithPage: ScheduleListResult;
  getSchedule: Schedule;
  getCurrentUser: User;
  getUsers?: Maybe<Array<User>>;
  getWorkplaces?: Maybe<Array<Workplace>>;
  getWorkplace: Workplace;
};


export type QueryGetAdvantageArgs = {
  _id: Scalars['ID'];
};


export type QueryGetArticlesArgs = {
  page: Scalars['Int'];
  limit: Scalars['Int'];
  type?: Maybe<ArticleType>;
  search?: Maybe<Scalars['String']>;
};


export type QueryGetArticleArgs = {
  _id: Scalars['ID'];
};


export type QueryGetArticlesByTypeArgs = {
  from?: Maybe<Scalars['String']>;
  limit: Scalars['Int'];
  type: ArticleType;
};


export type QueryGetCategoriesArgs = {
  parentId?: Maybe<Scalars['String']>;
};


export type QueryGetCategoryArgs = {
  _id: Scalars['ID'];
};


export type QueryGetCommentsArgs = {
  articleId: Scalars['ID'];
};


export type QueryGetCommentsWithSearchArgs = {
  page: Scalars['Int'];
  limit: Scalars['Int'];
  search?: Maybe<Scalars['String']>;
  articleId?: Maybe<Scalars['ID']>;
};


export type QueryGetEmployeesArgs = {
  search?: Maybe<Scalars['String']>;
  positionId?: Maybe<Scalars['ID']>;
  page: Scalars['Int'];
  limit: Scalars['Int'];
};


export type QueryGetEmployeeArgs = {
  _id: Scalars['ID'];
};


export type QueryGetGalleriesArgs = {
  category?: Maybe<Scalars['String']>;
};


export type QueryGetGalleryArgs = {
  category: Scalars['String'];
};


export type QueryGetImagesArgs = {
  page: Scalars['Int'];
  limit: Scalars['Int'];
};


export type QueryGetLessonsArgs = {
  categoryId?: Maybe<Scalars['ID']>;
};


export type QueryGetLessonsWithPageArgs = {
  page: Scalars['Int'];
  limit: Scalars['Int'];
  categoryId?: Maybe<Scalars['ID']>;
};


export type QueryGetLessonArgs = {
  _id: Scalars['ID'];
};


export type QueryGetReasonArgs = {
  _id: Scalars['ID'];
};


export type QueryGetRegistrationsArgs = {
  page: Scalars['Int'];
  limit: Scalars['Int'];
};


export type QueryGetRegistrationArgs = {
  _id: Scalars['ID'];
};


export type QueryGetSchedulesArgs = {
  categoryId?: Maybe<Scalars['ID']>;
  startDate?: Maybe<Scalars['Float']>;
};


export type QueryGetSchedulesWithPageArgs = {
  page: Scalars['Int'];
  limit: Scalars['Int'];
  categoryId?: Maybe<Scalars['ID']>;
  startDate?: Maybe<Scalars['Float']>;
};


export type QueryGetScheduleArgs = {
  _id: Scalars['ID'];
};


export type QueryGetWorkplaceArgs = {
  _id: Scalars['ID'];
};

export type Reason = {
  __typename?: 'Reason';
  _id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  body: Scalars['String'];
  image: Image;
  updatedBy?: Maybe<User>;
  createdBy: User;
  updatedAt?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
};

export type RegisterInput = {
  lastName: Scalars['String'];
  firstName: Scalars['String'];
  registerNo: Scalars['String'];
  phoneNumber?: Maybe<Array<Scalars['String']>>;
  dateOfBirth: Scalars['Float'];
  sex: Sex;
  email: Scalars['String'];
  facebookAccount: Scalars['String'];
  status: Scalars['String'];
  schoolName?: Maybe<Scalars['String']>;
  language: Scalars['String'];
  languagePoint: Scalars['String'];
  languageLevel: Scalars['String'];
  informationFrom: Scalars['String'];
  informationChannel: Scalars['String'];
};

export type Registration = {
  __typename?: 'Registration';
  _id?: Maybe<Scalars['ID']>;
  lastName: Scalars['String'];
  firstName: Scalars['String'];
  registerNo: Scalars['String'];
  phoneNumber?: Maybe<Array<Scalars['String']>>;
  dateOfBirth: Scalars['Float'];
  sex: Sex;
  email: Scalars['String'];
  facebookAccount: Scalars['String'];
  status: Scalars['String'];
  schoolName?: Maybe<Scalars['String']>;
  language: Scalars['String'];
  languagePoint: Scalars['String'];
  languageLevel: Scalars['String'];
  informationFrom: Scalars['String'];
  informationChannel: Scalars['String'];
  createdAt: Scalars['Float'];
};

export type RegistrationListResult = {
  __typename?: 'RegistrationListResult';
  registrations?: Maybe<Array<Registration>>;
  totalPages: Scalars['Int'];
};

export type Schedule = {
  __typename?: 'Schedule';
  _id?: Maybe<Scalars['ID']>;
  startDate: Scalars['Float'];
  lesson: Lesson;
  times: Array<Time>;
  category: Category;
};

export type ScheduleListResult = {
  __typename?: 'ScheduleListResult';
  schedules?: Maybe<Array<Schedule>>;
  totalPages: Scalars['Int'];
};

export enum Sex {
  Male = 'MALE',
  Female = 'FEMALE'
}

export type Time = {
  __typename?: 'Time';
  startTime: Scalars['String'];
  type: Scalars['String'];
};

export type User = {
  __typename?: 'User';
  _id?: Maybe<Scalars['ID']>;
  username: Scalars['String'];
  firstname?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  position?: Maybe<Position>;
};

export type Workplace = {
  __typename?: 'Workplace';
  _id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  goal?: Maybe<Scalars['String']>;
  generalRequirements?: Maybe<Array<Scalars['String']>>;
  requirements?: Maybe<Array<Scalars['String']>>;
  expiryDate: Scalars['Float'];
  updatedBy?: Maybe<User>;
  createdBy: User;
  updatedAt?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
};

export type GetAllEmployeesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllEmployeesQuery = (
  { __typename?: 'Query' }
  & { getAllEmployees?: Maybe<Array<(
    { __typename?: 'Employee' }
    & Pick<Employee, '_id' | 'name' | 'bio' | 'highlight'>
    & { image: (
      { __typename?: 'Image' }
      & Pick<Image, '_id' | 'path'>
    ) }
  )>> }
);

export type GetReasonsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetReasonsQuery = (
  { __typename?: 'Query' }
  & { getReasons?: Maybe<Array<(
    { __typename?: 'Reason' }
    & Pick<Reason, '_id' | 'name' | 'body'>
    & { image: (
      { __typename?: 'Image' }
      & Pick<Image, '_id' | 'path'>
    ) }
  )>> }
);

export type GetAdvantagesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAdvantagesQuery = (
  { __typename?: 'Query' }
  & { getAdvantages?: Maybe<Array<(
    { __typename?: 'Advantage' }
    & Pick<Advantage, '_id' | 'name'>
    & { image: (
      { __typename?: 'Image' }
      & Pick<Image, '_id' | 'path'>
    ) }
  )>> }
);

export type GetArticlesByTypeQueryVariables = Exact<{
  from: Scalars['String'];
  limit: Scalars['Int'];
  type: ArticleType;
}>;


export type GetArticlesByTypeQuery = (
  { __typename?: 'Query' }
  & { getArticlesByType?: Maybe<Array<(
    { __typename?: 'Article' }
    & Pick<Article, '_id' | 'title' | 'body' | 'commentsCount' | 'description' | 'type' | 'createdAt'>
    & { image: (
      { __typename?: 'Image' }
      & Pick<Image, '_id' | 'path'>
    ) }
  )>> }
);

export type GetArticleQueryVariables = Exact<{
  _id: Scalars['ID'];
}>;


export type GetArticleQuery = (
  { __typename?: 'Query' }
  & { getArticle: (
    { __typename?: 'Article' }
    & Pick<Article, '_id' | 'title' | 'body' | 'commentsCount' | 'description' | 'type' | 'createdAt'>
    & { image: (
      { __typename?: 'Image' }
      & Pick<Image, '_id' | 'path'>
    ), createdBy: (
      { __typename?: 'User' }
      & Pick<User, 'username'>
      & { position?: Maybe<(
        { __typename?: 'Position' }
        & Pick<Position, 'name'>
      )> }
    ) }
  ) }
);

export type GetCategoriesQueryVariables = Exact<{
  parentId?: Maybe<Scalars['String']>;
}>;


export type GetCategoriesQuery = (
  { __typename?: 'Query' }
  & { getCategories?: Maybe<Array<(
    { __typename?: 'Category' }
    & Pick<Category, '_id' | 'name' | 'tag' | 'color'>
    & { image: (
      { __typename?: 'Image' }
      & Pick<Image, '_id' | 'path'>
    ), icon: (
      { __typename?: 'Image' }
      & Pick<Image, '_id' | 'path'>
    ), parentCategory?: Maybe<(
      { __typename?: 'Category' }
      & Pick<Category, '_id' | 'name' | 'color'>
    )>, childrenCategories?: Maybe<Array<(
      { __typename?: 'Category' }
      & Pick<Category, '_id' | 'name' | 'color'>
      & { childrenCategories?: Maybe<Array<(
        { __typename?: 'Category' }
        & Pick<Category, '_id' | 'name' | 'color'>
      )>> }
    )>> }
  )>> }
);

export type GetCommentsQueryVariables = Exact<{
  articleId: Scalars['ID'];
}>;


export type GetCommentsQuery = (
  { __typename?: 'Query' }
  & { getComments?: Maybe<Array<(
    { __typename?: 'Comment' }
    & Pick<Comment, '_id' | 'author' | 'body'>
    & { article: (
      { __typename?: 'Article' }
      & Pick<Article, '_id'>
    ), replies?: Maybe<Array<(
      { __typename?: 'Comment' }
      & Pick<Comment, '_id' | 'author' | 'body'>
    )>> }
  )>> }
);

export type AddCommentMutationVariables = Exact<{
  articleId: Scalars['ID'];
  commentId?: Maybe<Scalars['ID']>;
  comment: AddCommentInput;
}>;


export type AddCommentMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'addComment'>
);

export type GetGalleryQueryVariables = Exact<{
  category: Scalars['String'];
}>;


export type GetGalleryQuery = (
  { __typename?: 'Query' }
  & { getGallery: (
    { __typename?: 'Gallery' }
    & Pick<Gallery, '_id' | 'name' | 'category'>
    & { images?: Maybe<Array<(
      { __typename?: 'Image' }
      & Pick<Image, '_id' | 'path'>
    )>> }
  ) }
);

export type GetLessonsQueryVariables = Exact<{
  categoryId?: Maybe<Scalars['ID']>;
}>;


export type GetLessonsQuery = (
  { __typename?: 'Query' }
  & { getLessons?: Maybe<Array<(
    { __typename?: 'Lesson' }
    & Pick<Lesson, '_id' | 'index' | 'name' | 'description' | 'price' | 'period' | 'level' | 'days' | 'studyTime' | 'studentLimit'>
    & { category: (
      { __typename?: 'Category' }
      & Pick<Category, '_id' | 'name' | 'tag' | 'color'>
      & { image: (
        { __typename?: 'Image' }
        & Pick<Image, '_id' | 'path'>
      ) }
    ) }
  )>> }
);

export type GetSchedulesQueryVariables = Exact<{
  categoryId?: Maybe<Scalars['ID']>;
}>;


export type GetSchedulesQuery = (
  { __typename?: 'Query' }
  & { getSchedules?: Maybe<Array<(
    { __typename?: 'Schedule' }
    & Pick<Schedule, '_id' | 'startDate'>
    & { lesson: (
      { __typename?: 'Lesson' }
      & Pick<Lesson, '_id' | 'name'>
    ), times: Array<(
      { __typename?: 'Time' }
      & Pick<Time, 'startTime' | 'type'>
    )>, category: (
      { __typename?: 'Category' }
      & Pick<Category, '_id' | 'name'>
    ) }
  )>> }
);

export type GetWorkplacesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetWorkplacesQuery = (
  { __typename?: 'Query' }
  & { getWorkplaces?: Maybe<Array<(
    { __typename?: 'Workplace' }
    & Pick<Workplace, '_id' | 'name' | 'goal' | 'generalRequirements' | 'requirements' | 'expiryDate'>
  )>> }
);


export const GetAllEmployeesDocument = gql`
    query getAllEmployees {
  getAllEmployees {
    _id
    name
    bio
    highlight
    image {
      _id
      path
    }
  }
}
    `;
export const GetReasonsDocument = gql`
    query getReasons {
  getReasons {
    _id
    name
    body
    image {
      _id
      path
    }
  }
}
    `;
export const GetAdvantagesDocument = gql`
    query getAdvantages {
  getAdvantages {
    _id
    name
    image {
      _id
      path
    }
  }
}
    `;
export const GetArticlesByTypeDocument = gql`
    query getArticlesByType($from: String!, $limit: Int!, $type: ArticleType!) {
  getArticlesByType(from: $from, limit: $limit, type: $type) {
    _id
    title
    body
    commentsCount
    description
    image {
      _id
      path
    }
    type
    createdAt
  }
}
    `;
export const GetArticleDocument = gql`
    query getArticle($_id: ID!) {
  getArticle(_id: $_id) {
    _id
    title
    body
    commentsCount
    description
    image {
      _id
      path
    }
    type
    createdAt
    createdBy {
      username
      position {
        name
      }
    }
  }
}
    `;
export const GetCategoriesDocument = gql`
    query getCategories($parentId: String) {
  getCategories(parentId: $parentId) {
    _id
    name
    tag
    image {
      _id
      path
    }
    icon {
      _id
      path
    }
    color
    parentCategory {
      _id
      name
      color
    }
    childrenCategories {
      _id
      name
      color
      childrenCategories {
        _id
        name
        color
      }
    }
  }
}
    `;
export const GetCommentsDocument = gql`
    query getComments($articleId: ID!) {
  getComments(articleId: $articleId) {
    _id
    author
    body
    article {
      _id
    }
    replies {
      _id
      author
      body
    }
  }
}
    `;
export const AddCommentDocument = gql`
    mutation addComment($articleId: ID!, $commentId: ID, $comment: AddCommentInput!) {
  addComment(articleId: $articleId, commentId: $commentId, comment: $comment)
}
    `;
export const GetGalleryDocument = gql`
    query getGallery($category: String!) {
  getGallery(category: $category) {
    _id
    name
    images {
      _id
      path
    }
    category
  }
}
    `;
export const GetLessonsDocument = gql`
    query getLessons($categoryId: ID) {
  getLessons(categoryId: $categoryId) {
    _id
    index
    name
    description
    price
    period
    level
    days
    studyTime
    studentLimit
    category {
      _id
      name
      tag
      image {
        _id
        path
      }
      color
    }
  }
}
    `;
export const GetSchedulesDocument = gql`
    query getSchedules($categoryId: ID) {
  getSchedules(categoryId: $categoryId) {
    _id
    startDate
    lesson {
      _id
      name
    }
    times {
      startTime
      type
    }
    category {
      _id
      name
    }
  }
}
    `;
export const GetWorkplacesDocument = gql`
    query getWorkplaces {
  getWorkplaces {
    _id
    name
    goal
    generalRequirements
    requirements
    expiryDate
  }
}
    `;

export type SdkFunctionWrapper = <T>(action: () => Promise<T>) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = sdkFunction => sdkFunction();
export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    getAllEmployees(variables?: GetAllEmployeesQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetAllEmployeesQuery> {
      return withWrapper(() => client.request<GetAllEmployeesQuery>(GetAllEmployeesDocument, variables, requestHeaders));
    },
    getReasons(variables?: GetReasonsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetReasonsQuery> {
      return withWrapper(() => client.request<GetReasonsQuery>(GetReasonsDocument, variables, requestHeaders));
    },
    getAdvantages(variables?: GetAdvantagesQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetAdvantagesQuery> {
      return withWrapper(() => client.request<GetAdvantagesQuery>(GetAdvantagesDocument, variables, requestHeaders));
    },
    getArticlesByType(variables: GetArticlesByTypeQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetArticlesByTypeQuery> {
      return withWrapper(() => client.request<GetArticlesByTypeQuery>(GetArticlesByTypeDocument, variables, requestHeaders));
    },
    getArticle(variables: GetArticleQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetArticleQuery> {
      return withWrapper(() => client.request<GetArticleQuery>(GetArticleDocument, variables, requestHeaders));
    },
    getCategories(variables?: GetCategoriesQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetCategoriesQuery> {
      return withWrapper(() => client.request<GetCategoriesQuery>(GetCategoriesDocument, variables, requestHeaders));
    },
    getComments(variables: GetCommentsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetCommentsQuery> {
      return withWrapper(() => client.request<GetCommentsQuery>(GetCommentsDocument, variables, requestHeaders));
    },
    addComment(variables: AddCommentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AddCommentMutation> {
      return withWrapper(() => client.request<AddCommentMutation>(AddCommentDocument, variables, requestHeaders));
    },
    getGallery(variables: GetGalleryQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetGalleryQuery> {
      return withWrapper(() => client.request<GetGalleryQuery>(GetGalleryDocument, variables, requestHeaders));
    },
    getLessons(variables?: GetLessonsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetLessonsQuery> {
      return withWrapper(() => client.request<GetLessonsQuery>(GetLessonsDocument, variables, requestHeaders));
    },
    getSchedules(variables?: GetSchedulesQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetSchedulesQuery> {
      return withWrapper(() => client.request<GetSchedulesQuery>(GetSchedulesDocument, variables, requestHeaders));
    },
    getWorkplaces(variables?: GetWorkplacesQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetWorkplacesQuery> {
      return withWrapper(() => client.request<GetWorkplacesQuery>(GetWorkplacesDocument, variables, requestHeaders));
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;