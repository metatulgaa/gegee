import useSWR from "swr";

import sdk from "../sdk";

const useGallery = ({ category }: { category: string }) => {
  const getGallery = useSWR(
    ["getGallery", category],
    async (_, gallery) =>
      (await sdk.getGallery({ category: gallery })).getGallery
  );

  return {
    gallery: getGallery.data,
  };
};

export default useGallery;
