import useSWR from "swr";

import sdk from "../sdk";
import { GetCategoriesQuery } from "../graphql";

export type Categories = GetCategoriesQuery["getCategories"];

const useCategories = () => {
  const getCategories = useSWR(
    "getCategories",
    async () => (await sdk.getCategories()).getCategories
  );

  return {
    categories: getCategories.data,
    loading: !getCategories.data && !getCategories.error,
  };
};

export default useCategories;
