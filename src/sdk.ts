import { GraphQLClient } from "graphql-request";

import { getSdk } from "./graphql";

export interface GraphQLError extends Error {
  response: {
    errors: {
      message: string;
    }[];
  };
}

const endpoint = `https://cloud.waves.mn/gegee/graphql`;

export const client = new GraphQLClient(endpoint);

const sdk = getSdk(client);

export default sdk;
