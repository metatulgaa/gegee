export const EnglishTestData = [
  {
    name: "Question 1-2",
    type: "QA",
    test: [
      {
        id: 0,
        question: "What is Jane doing now?",
        trueKey: 0,
        answers: [
          { key: 0, answer: "She's reading book" },
          { key: 1, answer: "She reads a book." },
          { key: 2, answer: "She's book reading." },
          { key: 3, answer: "She has a book reading." },
        ],
      },
      {
        id: 1,
        question:
          "She went to the shopping mall, but she _ _ _ _ _ any clothes.",
        trueKey: 0,
        answers: [
          { key: 0, answer: "No buys" },
          { key: 1, answer: "Doesn't buy" },
          { key: 2, answer: "Didn't buy" },
          { key: 3, answer: "Didn't bought" },
        ],
      },
    ],
  },
  {
    name: "Question 3-5",
    type: "QMA",
    question:
      "Dear Joanna\n I would like to send you ...3... . My brother, Tom, took it, who of course ...4... . As you can see it was ...5... . We don't like television, and we usually spend the time together in the living room.",
    imgPath: "/logo.png",
    test: [
      {
        id: 2,
        trueKey: 0,
        answers: [
          { key: 0, answer: "is reading a book" },
          { key: 1, answer: "is reading a book" },
          { key: 2, answer: "is reading a book" },
          { key: 3, answer: "is reading a book" },
        ],
      },
      {
        id: 3,
        trueKey: 0,
        answers: [
          { key: 0, answer: "is reading a book" },
          { key: 1, answer: "is reading a book" },
          { key: 2, answer: "is reading a book" },
          { key: 3, answer: "is reading a book" },
        ],
      },
      {
        id: 4,
        trueKey: 0,
        answers: [
          { key: 0, answer: "is reading a book" },
          { key: 1, answer: "is reading a book" },
          { key: 2, answer: "is reading a book" },
          { key: 3, answer: "is reading a book" },
        ],
      },
    ],
  },
  {
    name: "Question 6-8",
    type: "IA",
    test: [
      {
        id: 5,
        imgPath: "/logo.png",
        trueKey: 0,
        answers: [
          { key: 0, answer: "She's reading book" },
          { key: 1, answer: "She reads a book." },
          { key: 2, answer: "She's book reading." },
          { key: 3, answer: "She has a book reading." },
        ],
      },
      {
        id: 6,
        imgPath: "/logo.png",
        trueKey: 0,
        answers: [
          { key: 0, answer: "No buys" },
          { key: 1, answer: "Doesn't buy" },
          { key: 2, answer: "Didn't buy" },
          { key: 3, answer: "Didn't bought" },
        ],
      },
      {
        id: 7,
        imgPath: "/logo.png",
        trueKey: 0,
        answers: [
          { key: 0, answer: "She's reading book" },
          { key: 1, answer: "She reads a book." },
          { key: 2, answer: "She's book reading." },
          { key: 3, answer: "She has a book reading." },
        ],
      },
    ],
  },
];
