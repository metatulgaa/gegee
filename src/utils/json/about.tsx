export const aboutCardJson = [
  {
    id: 1,
    imgPath: "/assets/about/history.png",
    title: "Болдбаатарын Билгүүн",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  },
  {
    id: 2,
    imgPath: "/assets/about/history.png",
    title: "Д.Болдбаартар",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  },
  {
    id: 3,
    imgPath: "/assets/about/history.png",
    title: "Товч түүх",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  },
];

export const advJson = [
  {
    id: 1,
    title: "Хэл сурах арга барилд сургадаг",
    imgPath: "/assets/about/adv/1.png",
  },
  {
    id: 2,
    title: "Маш богино хугацаанд үр дүнгээ мэдэрдэг",
    imgPath: "/assets/about/adv/2.png",
  },
  {
    id: 3,
    title: "Цөөн хүнтэй тул хүн бүртэй тулж ажилдаг",
    imgPath: "/assets/about/adv/3.png",
  },
  {
    id: 4,
    title: "Анхаарал төвлрөх чадварт сургадаг",
    imgPath: "/assets/about/adv/4.png",
  },
  {
    id: 5,
    title:
      "Ном унших хөтөлбөрөөр тухайн хэлний бүх чадварыг системтэйгээр гүнзгийрүүлэн сайжруулдаг",
    imgPath: "/assets/about/adv/5.png",
  },
  {
    id: 6,
    title: "Чадварлаг багш нартай",
    imgPath: "/assets/about/adv/6.png",
  },
  {
    id: 7,
    title: "Танхимын сургалт дүрмийг дангаар зааж залхаахгүй",
    imgPath: "/assets/about/adv/7.png",
  },
  {
    id: 8,
    title: "Танхимын сургалт дүрмийг дангаар зааж залхаахгүй",
    imgPath: "/assets/about/adv/8.png",
  },
  { id: 9, title: "Гадаад багштай", imgPath: "/assets/about/adv/9.png" },
  {
    id: 10,
    title: "Тав тухтай орчин сургалтын олон төрлийн хэрэгсэл ашигладаг",
    imgPath: "/assets/about/adv/10.png",
  },
];
