export const scheduleData = [
  {
    date: "3 сарын 15нд",
    title: "эхлэх ангиуд",
    lessons: [
      {
        name: "Англи хэл",
        schedule: [
          { name: "Level 1", times: [{ time: "19:00-21:00" }] },
          {
            name: "Level 2",
            times: [
              { time: "17:00-19:00" },
              { time: "19:00-21:00" },
              { time: "19:00-21:00" },
            ],
          },
          {
            name: "Level 3",
            times: [{ time: "17:00-19:00" }],
          },
        ],
      },
      {
        name: "Хятад хэл",
        schedule: [
          { name: "Level 1", times: [{ time: "19:00-21:00" }] },
          {
            name: "Level 2",
            times: [
              { time: "17:00-19:00" },
              { time: "19:00-21:00" },
              { time: "19:00-21:00" },
            ],
          },
          {
            name: "Level 3",
            times: [{ time: "17:00-19:00" }],
          },
        ],
      },
    ],
  },
];
