export const homeMenu = [
  {
    title: "Түвшин тогтоох",
    imgPath: "/assets/icons/level.svg",
    description: "Онлайн тест",
    pathname: "/test",
  },
  {
    title: "Бүртгүүлэх",
    imgPath: "/assets/icons/register.svg",
    description: "Сургалтад бүртгүүлэх",
    pathname:
      "https://docs.google.com/forms/d/1NFPFny6SnxRasSQnfcAUlKPGby-JwRT2iYdO5Nmr5tE/viewform?ts=5fe32537&gxids=7628&fbclid=IwAR1_OzrrPsDMFibJubq1y_BDpkBERKgMbjknG9K68egXT3ovs5-_zR8x5cA&edit_requested=true",
  },
  {
    title: "Хаяг байршил",
    imgPath: "/assets/icons/location.svg",
    description: "Холбоо барих",
    pathname: "/about/2",
  },
];

export const lessonsMenu = [
  {
    title: "Англи хэл",
    imgPath: "/assets/icons/english.svg",
    description: "Хугацаа төлбөрийн мэдээлэл",
    pathname: "/lessons/english",
  },
  {
    title: "Хятад хэл",
    imgPath: "/assets/icons/chinese.svg",
    description: "Хугацаа төлбөрийн мэдээлэл",
    pathname: "/lessons/chinese",
  },
  {
    title: "Хүүхэд",
    imgPath: "/assets/icons/child.svg",
    description: "Сургалтын талаарх мэдээлэл",
    pathname: "/lessons/child",
  },
  {
    title: "Түвшин тогтоох",
    imgPath: "/assets/icons/level.svg",
    description: "Онлайн тест",
    pathname: "/test",
  },
];
