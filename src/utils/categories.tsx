export const StaticCategories = [
  {
    key: 2,
    path: "/schedule",
    title: "Хичээлийн хуваарь",
    submenu: [
      {
        title: "Хичээлийн \nхуваарь",
        imgPath: "/assets/icons/schedule.svg",
        pathname: "/schedule",
      },
    ],
  },
  {
    key: 3,
    path: "/about/0",
    title: "Яаагаад гэгээ вэ?",
    submenu: [
      {
        title: "Гэгээг сонгох\n шалтгаан",
        imgPath: "/assets/icons/choose.svg",
        pathname: "/about/0",
      },
      {
        title: "Бидний\n тухай",
        imgPath: "/assets/icons/about.svg",
        pathname: "/about/1",
      },
      {
        title: "Хаяг\n байршил",
        imgPath: "/assets/icons/location.svg",
        pathname: "/about/2",
      },
    ],
  },
  {
    key: 4,
    path: "/gegeeTalks",
    title: "Gegee talks",
    submenu: [
      {
        title: "Gegee talks",
        imgPath: "/assets/icons/gegeeTalks.svg",
        pathname: "/gegeeTalks",
      },
    ],
  },
  {
    key: 5,
    path: "/advice",
    title: "Зөвлөгөө",
    submenu: [
      {
        title: "Гэгээг зөвлөж\n байна",
        imgPath: "/assets/icons/advice.svg",
        pathname: "/advice",
      },
    ],
  },
  {
    key: 6,
    path: "/workplace",
    title: "Ажлын байр",
    submenu: [
      {
        title: "Нээлттэй\n ажлын байр",
        imgPath: "/assets/icons/job.svg",
        pathname: "/workplace",
      },
    ],
  },
];
