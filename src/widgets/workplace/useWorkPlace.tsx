import { useEffect } from "react";
import { useState } from "react";
import { GetWorkplacesQuery } from "../../graphql";
import sdk from "../../sdk";

export type WorksList = GetWorkplacesQuery["getWorkplaces"];

const useWorkPlace = () => {
  const [loading, setLoading] = useState(false);
  const [works, setWorks] = useState<WorksList>();

  useEffect(() => {
    setLoading(true);
    sdk
      .getWorkplaces()
      .then((res) => setWorks(res.getWorkplaces))
      .catch(() => {})
      .finally(() => setLoading(false));
  }, []);

  return { works, loading };
};

export default useWorkPlace;
