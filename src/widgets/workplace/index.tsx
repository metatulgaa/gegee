import styled from "styled-components";
import { Grid } from "@material-ui/core";

import ChildCollapse from "../../components/ChildCollapse";
import { Loading } from "../../components/Loading";
import NoData from "../../components/NoData";
import useWorkPlace from "./useWorkPlace";

const Button = styled.button`
  text-transform: uppercase;
  color: #ffffff;
  border-radius: 10px;
  border: 0;
  background-color: rgb(255, 166, 0);
  padding: 6px 12px;
  outline: none;
  z-index: 10;
  cursor: pointer;
`;

export const WorkplaceWidget = ({
  galleryLoading,
}: {
  galleryLoading: boolean;
}) => {
  const { works, loading } = useWorkPlace();

  return (
    <div className="workplace">
      {loading || (galleryLoading && <Loading />)}
      <Grid container spacing={2}>
        {works?.length !== 0 ? (
          works?.map((item) => (
            <Grid item xs={12} key={item._id}>
              <ChildCollapse color="rgb(255, 166, 0)" title={item.name}>
                <h3 style={{ fontWeight: 100 }}>Ажлын байрны зорилго</h3>
                <p>{item.goal}</p>

                <h3>Ерөнхий шаардлага</h3>
                <>
                  {item?.generalRequirements?.map((text) => (
                    <p key={text}>-{text}</p>
                  ))}
                </>

                <h3>Тавигдах шаардлага</h3>
                <>
                  {item?.requirements?.map((text) => (
                    <p key={text}>-{text}</p>
                  ))}
                </>

                <Button type="button">Анкет бөглөх</Button>
              </ChildCollapse>
            </Grid>
          ))
        ) : (
          <NoData />
        )}
      </Grid>
    </div>
  );
};
