import { useState } from "react";
import { Container, Grid } from "@material-ui/core";

import { Section } from "../../components/Section";
import ImageAnswer from "../../components/Test/ImageAnswer";
import OneQuestionManyAnswer from "../../components/Test/OneQuestionManyAnswer";
import QuestionAnswer from "../../components/Test/QuestionAnswer";
import { EnglishTestData } from "../../utils/test/english";

export type AnswerResult = {
  testId: number;
  answerKey: number;
};

export type Result = {
  testId: number;
  result: boolean;
  answerKey: number;
  trueKey: number;
};

const TestWidget = () => {
  const [answerResult, setAnswerResult] = useState<AnswerResult[]>();
  const [result, setResult] = useState<Result[]>();

  const endTestClick = () => {
    const obj = [];
    const allTest = [];
    EnglishTestData.forEach((item) =>
      item.test.forEach((t) => allTest.push(t))
    );
    if (answerResult && answerResult.length === allTest.length) {
      answerResult.forEach((item) =>
        EnglishTestData.forEach((el) => {
          for (var i = 0; i < el.test.length; i++) {
            if (item.testId === el.test[i].id) {
              if (el.test[i].trueKey === item.answerKey) {
                obj.push({
                  testId: item.testId,
                  result: true,
                  answerKey: item.answerKey,
                  trueKey: el.test[i].trueKey,
                });
              } else {
                obj.push({
                  testId: item.testId,
                  result: false,
                  answerKey: item.answerKey,
                  trueKey: el.test[i].trueKey,
                });
              }
            }
          }
        })
      );
      setResult(obj);
    } else {
      alert("Хариултаа дуустал бөглөнө үү");
    }
  };

  const handleChange = ({
    testId,
    answerKey,
  }: {
    testId: number;
    answerKey: number;
  }) => {
    if (answerResult) {
      const check = answerResult?.find((item) => item.testId === testId);
      if (check) {
        const removedList = answerResult?.filter(
          (ans) => ans.testId !== testId
        );
        setAnswerResult([
          ...removedList?.map((item) => item),
          { testId, answerKey },
        ]);
      } else {
        setAnswerResult([...answerResult, { testId, answerKey }]);
      }
    } else {
      setAnswerResult([{ testId, answerKey }]);
    }
  };

  const RenderSections = (data: any) => {
    switch (data.type) {
      case "QA": {
        return (
          <Section padding="36px 0">
            <Container maxWidth="md">
              <QuestionAnswer
                test={data.test}
                name={data.name}
                result={result}
                answerResult={answerResult}
                handleChange={handleChange}
              />
            </Container>
          </Section>
        );
      }
      case "QMA": {
        return (
          <Section
            padding="36px 0"
            color="rgb(247,247,247)"
            setAnswerResult={setAnswerResult}
          >
            <Container maxWidth="md">
              <OneQuestionManyAnswer
                test={data.test}
                name={data.name}
                result={result}
                question={data.question}
                imgPath={data.imgPath}
                answerResult={answerResult}
                handleChange={handleChange}
              />
            </Container>
          </Section>
        );
      }
      case "IA": {
        return (
          <Section padding="36px 0">
            <Container maxWidth="md">
              <ImageAnswer
                test={data.test}
                name={data.name}
                result={result}
                handleChange={handleChange}
                answerResult={answerResult}
              />
            </Container>
          </Section>
        );
      }
    }
  };

  return (
    <>
      {EnglishTestData.map((item) => RenderSections(item))}
      {!result && (
        <Container maxWidth="md">
          <button onClick={endTestClick} className="end_button">
            Дуусгах
          </button>
        </Container>
      )}
      {result && result.length !== 0 && (
        <Container maxWidth="md">
          <Grid container justify="flex-end">
            <Grid item xs={12} md={10}>
              <div className="test_bottom">
                <div className="test_result">
                  {result.length}/
                  {result.filter((item) => item.result === true)?.length}
                </div>
                <div className="test_text">
                  Таны суралцах түвшин - Англи хэл 4
                </div>
                <a
                  target="_blank"
                  href="https://docs.google.com/forms/d/1NFPFny6SnxRasSQnfcAUlKPGby-JwRT2iYdO5Nmr5tE/viewform?ts=5fe32537&gxids=7628&fbclid=IwAR1_OzrrPsDMFibJubq1y_BDpkBERKgMbjknG9K68egXT3ovs5-_zR8x5cA&edit_requested=true"
                >
                  Бүртгүүлэх
                </a>
              </div>
            </Grid>
          </Grid>
        </Container>
      )}
    </>
  );
};

export default TestWidget;
