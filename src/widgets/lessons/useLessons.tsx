import { useEffect } from "react";
import { useState } from "react";
import { GetLessonsQuery } from "../../graphql";
import sdk from "../../sdk";

export type LessonsList = GetLessonsQuery["getLessons"];

const useLessons = ({ id }: { id: string }) => {
  const [loading, setLoading] = useState(false);
  const [lessons, setLessons] = useState<LessonsList>();

  useEffect(() => {
    if (id) {
      setLoading(true);
      sdk
        .getLessons({ categoryId: id })
        .then((res) => setLessons(res.getLessons))
        .catch(() => {})
        .finally(() => setLoading(false));
    }
  }, [id]);

  return { loading, lessons };
};

export default useLessons;
