import { Grid } from "@material-ui/core";

import { Section } from "../../components/Section";
import Collapse from "../../components/Collapse";
import NoData from "../../components/NoData";
import useLessons from "./useLessons";
import CollapseLoading from "../../components/Loadings/CollapseLoading";

const ChildChildLessonWidget = ({
  categoryId,
  name,
  color,
}: {
  categoryId: string;
  name: string;
  color?: string;
}) => {
  const { lessons, loading } = useLessons({ id: categoryId });
  return (
    <Section color="#fff" padding="26px">
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <h3 style={{ margin: 0, color: color }}>{name}</h3>
        </Grid>
        {!lessons || (lessons.length === 0 && !loading && <NoData />)}
        {lessons?.map((item) => (
          <Grid item xs={12} key={item._id}>
            <Collapse
              index={item.index}
              color="rgb(255, 166, 0)"
              title={item.name}
              description={item.description}
              data={[
                { title: "Хугацаа", value: item?.period.toString() },
                { title: "Хичээллэх өдөр", value: item?.days.toString() },
                {
                  title: "Хичээллэх цаг",
                  value: item?.studyTime.toString(),
                },
                {
                  title: "Нэг ангид",
                  value: item?.studentLimit.toString(),
                },
                { title: "Хүрэх түвшин", value: "defualt" },
                { title: "Төлбөр", value: item?.price.toString() },
              ]}
            />
          </Grid>
        ))}
        {loading && (
          <>
            <Grid item xs={12}>
              <CollapseLoading />
            </Grid>
            <Grid item xs={12}>
              <CollapseLoading />
            </Grid>
            <Grid item xs={12}>
              <CollapseLoading />
            </Grid>
          </>
        )}
      </Grid>
    </Section>
  );
};

export default ChildChildLessonWidget;
