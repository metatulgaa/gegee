import { Container, Grid } from "@material-ui/core";

import { Section } from "../../components/Section";
import Collapse from "../../components/Collapse";
import NoData from "../../components/NoData";
import useLessons from "./useLessons";
import CollapseLoading from "../../components/Loadings/CollapseLoading";

const ChildLessonsWidget = ({
  categoryId,
  color,
}: {
  categoryId: string;
  color: string;
}) => {
  const { lessons, loading } = useLessons({ id: categoryId });

  return (
    <>
      <Grid container spacing={2}>
        {!lessons || (lessons.length === 0 && !loading && <NoData />)}
        {lessons?.map((item) => (
          <Grid item xs={12} key={item._id}>
            <Collapse
              index={item.index}
              color={color}
              title={item.name}
              description={item.description}
              data={[
                { title: "Хугацаа", value: item?.period.toString() },
                { title: "Хичээллэх өдөр", value: item?.days.toString() },
                {
                  title: "Хичээллэх цаг",
                  value: item?.studyTime.toString(),
                },
                {
                  title: "Нэг ангид",
                  value: item?.studentLimit.toString(),
                },
                { title: "Хүрэх түвшин", value: item.level },
                { title: "Төлбөр", value: item?.price.toString() },
              ]}
            />
          </Grid>
        ))}
        {loading && (
          <>
            <Grid item xs={12}>
              <CollapseLoading />
            </Grid>
            <Grid item xs={12}>
              <CollapseLoading />
            </Grid>
            <Grid item xs={12}>
              <CollapseLoading />
            </Grid>
          </>
        )}
      </Grid>
    </>
  );
};

export default ChildLessonsWidget;
