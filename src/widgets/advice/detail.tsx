import { Container } from "@material-ui/core";
import { format } from "date-fns";
import { ReactSVG } from "react-svg";

import { PostImage } from "../../components/Carousel";
import { FlexLayout } from "../../components/Header/Header";
import MarkUp from "../../components/MarkUp";
import { Section } from "../../components/Section";
import { OneArticle } from "../../pages/gegeeTalks/[id]";
import { CommentsWidget } from "../post/comments";

export const AdviceDetailWidget = ({
  initialData,
}: {
  initialData: OneArticle;
}) => {
  return (
    <>
      <Section padding="24px 0">
        <Container maxWidth="md">
          <div className="talk_post">
            <h1>{initialData.title}</h1>

            <FlexLayout style={{ justifyContent: "flex-start" }}>
              <p style={{ marginRight: 24 }}>
                <ReactSVG src="/assets/icons/clock.svg" />
                {format(new Date(initialData.createdAt), "YYYY-MM-DD")}
              </p>
              <p>
                <ReactSVG src="/assets/icons/writing.svg" />
                {initialData?.createdBy?.position?.name}
              </p>
            </FlexLayout>

            <PostImage src={`${process.env.API}${initialData?.image?.path}`} />

            <div
              className="editer_style"
              dangerouslySetInnerHTML={MarkUp(initialData.body)}
            />

            <FlexLayout style={{ justifyContent: "flex-start", marginTop: 24 }}>
              <a>
                <ReactSVG src="/assets/icons/comment-1.svg" />
                {initialData.commentsCount}
              </a>
            </FlexLayout>
          </div>
        </Container>
      </Section>
      <Section color="rgb(247,247,247)" padding="24px 0 48px 0">
        <Container maxWidth="md">
          <CommentsWidget articleId={initialData._id} />
        </Container>
      </Section>
    </>
  );
};
