import Link from "next/link";
import { Grid } from "@material-ui/core";
import { format } from "date-fns";

import { Post } from "../../components/Post";
import { Loading } from "../../components/Loading";
import NoData from "../../components/NoData";
import useAdvice from "./useAdvice";

export const AdviceWidget = () => {
  const { loading, advices } = useAdvice();

  return (
    <Grid container spacing={3}>
      {loading ? (
        <Loading />
      ) : advices !== null ? (
        advices?.map((item, index) => (
          <Grid item xs={12} key={item._id}>
            <Link href={`advice/${item._id}`}>
              <div>
                <Post
                  no={`№${index + 1}`}
                  title={item.title}
                  commendNo={item.commentsCount}
                  date={format(
                    new Date(item.createdAt),
                    "YYYY-MM-DD"
                  ).toString()}
                  description={item?.description}
                  imgPath={`${process.env.API}${item.image.path}`}
                />
              </div>
            </Link>
          </Grid>
        ))
      ) : (
        <NoData />
      )}
    </Grid>
  );
};
