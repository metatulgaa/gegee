import { useEffect } from "react";
import { useState } from "react";

import { ArticleType, GetArticlesByTypeQuery } from "../../graphql";
import sdk from "../../sdk";

export type Advices = GetArticlesByTypeQuery["getArticlesByType"];

const useAdvice = () => {
  const [loading, setLoading] = useState(false);
  const [advices, setAdvices] = useState<Advices>();

  useEffect(() => {
    setLoading(true);
    sdk
      .getArticlesByType({ from: "", limit: 10, type: ArticleType.Advice })
      .then((res) => setAdvices(res.getArticlesByType))
      .catch(() => {})
      .finally(() => setLoading(false));
  }, []);

  return { loading, advices };
};

export default useAdvice;
