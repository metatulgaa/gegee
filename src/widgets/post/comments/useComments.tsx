import { useEffect } from "react";
import { useState } from "react";
import { GetCommentsQuery } from "../../../graphql";
import sdk from "../../../sdk";

const useComments = ({ articleId }: { articleId: string }) => {
  const [comments, setComments] = useState<GetCommentsQuery["getComments"]>();
  const [loading, setLoading] = useState(false);

  const getComments = () => {
    setLoading(true);
    sdk
      .getComments({ articleId })
      .then((res) => setComments(res.getComments))
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    if (articleId) {
      getComments();
    }
  }, [articleId]);

  const addComment = (text: string) => {
    if (text && text !== "") {
      setLoading(true);
      sdk
        .addComment({
          articleId: articleId,
          comment: { author: "Зочин", body: text },
        })
        .then(() => getComments())
        .finally(() => setLoading(false));
    }
  };

  const addReply = ({
    text,
    commentId,
  }: {
    text: string;
    commentId: string;
  }) => {
    if (text && text !== "") {
      setLoading(true);
      sdk
        .addComment({
          articleId: articleId,
          comment: { author: "Зочин", body: text },
          commentId: commentId,
        })
        .then(() => getComments())
        .finally(() => setLoading(false));
    }
  };

  return { comments, loading, addComment, addReply };
};

export default useComments;
