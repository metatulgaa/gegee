import { Grid } from "@material-ui/core";
import { useState } from "react";
import { format } from "date-fns";

import { CommentItem, CommentLayout, Icon } from "../../../components/Comment";
import { Loading } from "../../../components/Loading";
import NoData from "../../../components/NoData";
import { Section } from "../../../components/Section";
import useComments from "./useComments";

export const CommentsWidget = ({ articleId }: { articleId: string }) => {
  const { comments, loading, addComment, addReply } = useComments({
    articleId,
  });
  const [value, setValue] = useState<string>();
  const [replyValue, setReplyValue] = useState<string>();
  const [reply, setReply] = useState<string>();

  return (
    <div className="comment">
      <p>АНХААР!</p>
      <p>
        Та сэтгэгдэл бичихдээ хууль зүйн болон ёс суртахууныг баримтална уу. Ёс
        бус сэтгэгдлийг админ устгах эрхтэй.
      </p>
      <div className="comment_input">
        <Grid item xs={2} sm={1}>
          <i className="fa fa-user" />
        </Grid>
        <Grid item xs={10} sm={11}>
          <div className="input">
            <input
              value={value}
              onChange={(event) => setValue(event.target.value)}
            />
            <button
              onClick={() => {
                addComment(value);
                setValue("");
              }}
            >
              Нийтлэх
            </button>
          </div>
        </Grid>
      </div>
      <Section padding="48px 0 0 0">
        <Grid container spacing={3} justify="flex-end">
          {comments?.length === 0 && <NoData />}
          {comments &&
            comments?.length !== 0 &&
            comments?.map((item) => (
              <>
                <Grid item xs={12} key={item?._id}>
                  <CommentLayout
                    style={{ borderTop: "1px solid #dedede", paddingTop: 12 }}
                  >
                    <Grid container>
                      <Grid item xs={2} sm={1}>
                        <Icon>
                          <i className="fa fa-user" />
                        </Icon>
                      </Grid>
                      <Grid item xs={10} sm={11}>
                        <h3>Зочин</h3>
                        <label>{item.body}</label>
                        <div>
                          <span>
                            <i className="fal fa-clock" />
                            2021-04-12
                          </span>

                          <a onClick={() => setReply(item._id)}>
                            <i className="fal fa-comment" />
                            Хариулах
                          </a>
                        </div>
                      </Grid>
                    </Grid>
                  </CommentLayout>
                </Grid>

                <Grid item xs={10} sm={11}>
                  <div>
                    {item.replies.map((rep) => (
                      <CommentLayout>
                        <Grid container spacing={2}>
                          <Grid item xs={2} sm={1}>
                            <Icon>
                              <i className="fa fa-user" />
                            </Icon>
                          </Grid>
                          <Grid item xs={10} sm={11}>
                            <h3>Зочин</h3>
                            <label>{rep.body}</label>
                            <div>
                              <span>
                                <i className="fal fa-clock" />
                                2021-04-12
                              </span>
                            </div>
                          </Grid>
                        </Grid>
                      </CommentLayout>
                    ))}
                  </div>
                  {reply === item._id && (
                    <div className="comment_input">
                      <Grid item xs={2} sm={1}>
                        <i className="fa fa-user" />
                      </Grid>
                      <Grid item xs={10} sm={11}>
                        <div className="input">
                          <input
                            value={replyValue}
                            onChange={(event) =>
                              setReplyValue(event.target.value)
                            }
                          />
                          <button
                            onClick={() => {
                              addReply({
                                text: replyValue,
                                commentId: item._id,
                              });
                              setReplyValue("");
                            }}
                          >
                            Нийтлэх
                          </button>
                        </div>
                      </Grid>
                    </div>
                  )}
                </Grid>
              </>
            ))}
        </Grid>
      </Section>
    </div>
  );
};
