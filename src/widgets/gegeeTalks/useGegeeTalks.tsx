import { useEffect } from "react";
import { useState } from "react";

import { ArticleType, GetArticlesByTypeQuery } from "../../graphql";
import sdk from "../../sdk";

export type GegeeTalks = GetArticlesByTypeQuery["getArticlesByType"];

const useGegeeTalks = () => {
  const [loading, setLoading] = useState(false);
  const [gegeeTalks, setGegeeTalks] = useState<GegeeTalks>();

  useEffect(() => {
    setLoading(true);
    sdk
      .getArticlesByType({ from: "", limit: 10, type: ArticleType.Talk })
      .then((res) => setGegeeTalks(res.getArticlesByType))
      .catch(() => {})
      .finally(() => setLoading(false));
  }, []);

  return { loading, gegeeTalks };
};

export default useGegeeTalks;
