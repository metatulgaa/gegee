import Link from "next/link";
import { Grid } from "@material-ui/core";
import { format } from "date-fns";

import { Post } from "../../components/Post";
import { Loading } from "../../components/Loading";
import NoData from "../../components/NoData";
import useGegeeTalks from "./useGegeeTalks";

export const GegeeTalksWidget = ({
  imageLoading,
}: {
  imageLoading: boolean;
}) => {
  const { loading, gegeeTalks } = useGegeeTalks();

  return (
    <Grid container spacing={3}>
      {loading || imageLoading ? (
        <Loading />
      ) : gegeeTalks !== null ? (
        gegeeTalks?.map((item, index) => (
          <Grid item xs={12} key={item._id}>
            <Link href={`/gegeeTalks/${item._id}`}>
              <div>
                <Post
                  no={`Gegee Talks #${index + 1}`}
                  title={item.title}
                  commendNo={item.commentsCount}
                  date={format(
                    new Date(item.createdAt),
                    "YYYY-MM-DD"
                  ).toString()}
                  description={item?.description}
                  imgPath={`${process.env.API}${item.image.path}`}
                />
              </div>
            </Link>
          </Grid>
        ))
      ) : (
        <NoData />
      )}
    </Grid>
  );
};
