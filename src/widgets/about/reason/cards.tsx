import { Grid } from "@material-ui/core";

import WhiteCardLoading from "../../../components/Loadings/WhiteCardLoading";
import WhiteCard from "../../../components/WhiteCard";
import { ReasonList } from "./useReason";

export const AboutReasonCardsWidget = ({
  reasons,
  loading,
}: {
  reasons: ReasonList;
  loading: boolean;
}) => {
  return (
    <Grid container spacing={3}>
      {loading
        ? [1, 2, 3].map((load) => (
            <Grid item xs={12} sm={4} key={load}>
              <WhiteCardLoading />
            </Grid>
          ))
        : reasons?.map((item) => (
            <Grid item xs={12} sm={4} key={item._id}>
              <WhiteCard
                title={item.name}
                description={item.body}
                imgPath={`${process.env.API}${item.image?.path}`}
              />
            </Grid>
          ))}
    </Grid>
  );
};
