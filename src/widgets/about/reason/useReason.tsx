import { useEffect, useState } from "react";
import { GetAdvantagesQuery, GetReasonsQuery } from "../../../graphql";
import sdk from "../../../sdk";

export type ReasonList = GetReasonsQuery["getReasons"];
export type AdvantageList = GetAdvantagesQuery["getAdvantages"];

export const useReason = () => {
  const [loading, setLoading] = useState(false);
  const [reasons, setReasons] = useState<ReasonList>();
  const [advatages, setAdvantages] = useState<AdvantageList>();

  useEffect(() => {
    setLoading(true);
    sdk
      .getReasons()
      .then((res) => setReasons(res.getReasons))
      .catch(() => {});

    sdk
      .getAdvantages()
      .then((res) => setAdvantages(res.getAdvantages))
      .catch(() => {})
      .finally(() => setLoading(false));
  }, []);

  return { reasons, loading, advatages };
};
