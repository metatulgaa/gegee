import { Container } from "@material-ui/core";

import { Section } from "../../../components/Section";

import { AboutReasonAdvantageWidget } from "./advantage";
import { AboutReasonCardsWidget } from "./cards";
import { AboutReasonMethodWidget } from "./method";
import { useReason } from "./useReason";

export const AboutReasonWidget = () => {
  const { loading, reasons, advatages } = useReason();
  return (
    <Section color="rgb(247,247,247)">
      <Section padding="48px 0">
        <Container>
          <AboutReasonCardsWidget reasons={reasons} loading={loading} />
        </Container>
      </Section>
      <Section color="rgb(255, 255, 255)" style={{ padding: "36px 0 48px 0" }}>
        <Container>
          <AboutReasonMethodWidget />
        </Container>
      </Section>
      <div
        style={{
          textAlign: "center",
        }}
      >
        <h1 style={{ color: "rgb(117, 117, 117)", fontWeight: 300 }}>
          Манай сургалтын давуу талууд:
        </h1>
      </div>
      <Section color="rgb(255, 255, 255)" style={{ padding: "48px 0" }}>
        <Container>
          <AboutReasonAdvantageWidget advatages={advatages} loading={loading} />
        </Container>
      </Section>
    </Section>
  );
};
