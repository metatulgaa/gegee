import styled from "styled-components";

const Layout = styled.div`
  h1 {
    margin: 0;
    font-weight: 300;
    color: rgb(255, 166, 0);
    text-align: center;
  }

  @media (max-width: 960px) {
  }
`;

const Text = styled.div`
  border-left: 4px solid orange;
  padding-left: 16px;
  color: rgb(117, 117, 117);
  margin-top: 24px;
  line-height: 25px;
  @media (max-width: 960px) {
  }
`;

export const AboutReasonMethodWidget = () => {
  return (
    <Layout>
      <h1>
        <strong>"Гэгээ" </strong>
        боловсролын төвийн
        <strong> арга барил</strong>
      </h1>
      <Text>
        Д.Болдбаатар багшийн 20 гаруй жил Хятад хэл судалж, оюутан сурагчдад
        хятад хэл заах явцдаа тухайн хэлийг хамгийн сайнаар сургах арга барилыг
        гаргаж ирсэн. Энэ нь ямарч хэлийг сурахдаа системтэйгээр, зөв дэс
        дараалалтайгаар, хурдан хугацаанд өндөр түвшинд хүрэхээс гадна хэл сурах
        арга барилыг эзэмшүүлэх юм. Энэхүү аргаа англи хэл, япон хэл дээр
        нэвтрүүлсэн байна.
      </Text>
      <Text>
        Зөвхөн гадаад хэлийг сураад зогсохгүй хувь хүнд хэрэгтэй олон
        чадваруудыг давхар хөгжүүлж байдаг.
      </Text>
    </Layout>
  );
};
