import { AdvantageList } from "./useReason";
import AdvantageLoading from "../../../components/Loadings/AdvantageLoading";

import styled from "styled-components";

const Table = styled.table`
  width: 100%;
  color: rgb(117, 117, 117);
  text-align: center;
  border-collapse: collapse;

  img {
    height: 80px;
  }
  tr {
    flex-wrap: wrap;
  }

  tr td {
    padding: 18px;
    border: 1px solid rgba(230, 230, 230);
    border-top: none;
    width: 20%;
  }

  tr:first-child td {
    border-top: none;
  }
  tr td:first-child {
    border-left: none;
  }
  tr:last-child td {
    border-bottom: none;
  }
  tr td:last-child {
    border-right: none;
  }

  @media (max-width: 960px) {
  }
`;

export const AboutReasonAdvantageWidget = ({
  advatages,
  loading,
}: {
  advatages: AdvantageList;
  loading: boolean;
}) => {
  return (
    <Table>
      {loading ? (
        <AdvantageLoading />
      ) : (
        <tbody>
          <tr>
            {advatages?.slice(0, 5).map((adv) => (
              <td key={adv._id}>
                <img
                  loading="lazy"
                  src={
                    adv.image.path
                      ? `${process.env.API}${adv.image.path}`
                      : "/logo.png"
                  }
                  alt={adv._id.toString()}
                />
                <p>{adv.name}</p>
              </td>
            ))}
          </tr>
          <tr>
            {advatages?.slice(5, 10).map((adv) => (
              <td key={adv._id}>
                <img
                  loading="lazy"
                  src="/assets/about/adv/1.png"
                  alt={adv._id.toString()}
                />
                <p>{adv.name}</p>
              </td>
            ))}
          </tr>
        </tbody>
      )}
    </Table>
  );
};
