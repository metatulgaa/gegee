import { Container, Grid } from "@material-ui/core";

import { Profile } from "../../components/Profile";
import WhiteCard from "../../components/WhiteCard";
import { Section } from "../../components/Section";
import WhiteCardLoading from "../../components/Loadings/WhiteCardLoading";
import ProfileLoading from "../../components/Loadings/ProfileLoading";
import { useAbout } from "./useAbout";
import PurposeWidget from "./purpose";

const AboutWidget = () => {
  const { employees, loading } = useAbout();
  return (
    <Section color="rgb(247,247,247)">
      <Section padding="48px 0">
        <Container>
          <Grid container spacing={3}>
            {loading
              ? [1, 2, 3].map((load) => (
                  <Grid item xs={12} sm={4} key={load}>
                    <WhiteCardLoading />
                  </Grid>
                ))
              : employees
                  ?.filter((emp) => emp.highlight)
                  .map((item) => (
                    <Grid item xs={12} sm={4} key={item._id}>
                      <WhiteCard
                        title={item.name}
                        description={item.bio}
                        imgPath={`${process.env.API}${item?.image?.path}`}
                      />
                    </Grid>
                  ))}
          </Grid>
        </Container>
      </Section>
      <Section color="rgb(255, 255, 255)" padding="36px 0 24px 0">
        <Container>
          <PurposeWidget />
        </Container>
      </Section>
      <div style={{ padding: "48px 0" }}>
        <Container>
          <h1
            style={{
              margin: 0,
              fontWeight: 300,
              color: "rgb(117, 117, 117)",
              textTransform: "uppercase",
              textAlign: "center",
              marginBottom: "48px",
            }}
          >
            Манай хамт олон
          </h1>
          <Grid container spacing={3}>
            {loading
              ? [1, 2, 3, 4, 5, 6].map((load) => (
                  <Grid item xs={6} sm={2} key={load}>
                    <ProfileLoading />
                  </Grid>
                ))
              : employees
                  ?.filter((emp) => !emp.highlight)
                  .map((item) => (
                    <Grid item xs={6} sm={2} key={item._id}>
                      <Profile
                        key={item._id}
                        imgPath={`${process.env.API}${item?.image?.path}`}
                        title={item.name}
                        description="Ерөнхий захирал"
                      />
                    </Grid>
                  ))}
          </Grid>
        </Container>
      </div>
    </Section>
  );
};

export default AboutWidget;
