import { useEffect, useState } from "react";
import { GetAllEmployeesQuery } from "../../graphql";

import sdk from "../../sdk";

export type EmployeeList = GetAllEmployeesQuery["getAllEmployees"];

export const useAbout = () => {
  const [employees, setEmployee] = useState<EmployeeList>();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    sdk
      .getAllEmployees()
      .then((res) => setEmployee(res.getAllEmployees))
      .catch(() => {})
      .finally(() => setLoading(false));
  }, []);

  return { employees, loading };
};
