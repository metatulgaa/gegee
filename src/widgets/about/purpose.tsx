import styled from "styled-components";

const Layout = styled.div`
  width: 100%;
  display: flex;

  @media (max-width: 960px) {
  }
`;

const Item = styled.div`
  width: 100%;
  h1 {
    margin: 0;
    font-weight: 300;
    color: rgb(255, 166, 0);
    text-transform: uppercase;
  }
  p {
    color: rgb(117, 117, 117);
    line-height: 25px;
  }

  @media (max-width: 960px) {
  }
`;

const Solid = styled.div`
  width: 1px;
  background-color: orange;
  margin: 0 24px;
  margin-top: -12px;
  @media (max-width: 960px) {
  }
`;

const PurposeWidget = () => {
  return (
    <Layout>
      <Item style={{ textAlign: "right" }}>
        <h1>Бидний зорилго</h1>
        <p>
          Бид өөрсдийн боловсруулсан онцгой заах арга барил,
          <br />
          маш сайн хөтөлбөрөөр дамжуулан олон мянган монгол
          <br />
          хүнд гадаад хэлийг богино хугацаанд, өндөр үр дүнтэйгээр
          <br />
          эзэмшүүлэх зорилготой.
        </p>
      </Item>
      <Solid />
      <Item>
        <h1>Бидний алсын хараа</h1>
        <p>
          Бид өөрсдийн боловсруулсан онцгой заах арга барил маш сайн хөтөлбөрөөр
          дамжуулан олон мянган монгол хүнд гадаад хэлийг богино хугацаанд,
          өндөр үр дүнтэйгээр эзэмшүүлэх зорилготой. Бид өөрсдийн боловсруулсан
          онцгой заах арга барил маш сайн хөтөлбөрөөр дамжуулан олон мянган
          монгол хүнд гадаад хэлийг богино хугацаанд, өндөр үр дүнтэйгээр
          эзэмшүүлэх зорилготой.
        </p>
      </Item>
    </Layout>
  );
};

export default PurposeWidget;
