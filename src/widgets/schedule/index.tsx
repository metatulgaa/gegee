import { Container, Grid } from "@material-ui/core";

import useSchedule from "./useSchedule";
import Social from "../../components/Social";
import Schedule from "../../components/Schedule";
import Breadcumb from "../../components/Breadcumb";
import { Loading } from "../../components/Loading";
import { CoverLayout } from "../../components/Carousel";
import { Section } from "../../components/Section";
import useGallery from "../../hooks/useGallery";
import NoData from "../../components/NoData";
import { useEffect, useState } from "react";

const SchedulesWidget = ({
  categoryId,
  breadCumbCats,
}: {
  categoryId?: string;
  breadCumbCats: any;
}) => {
  const { schedules, loading } = useSchedule({ categoryId });
  const { gallery } = useGallery({
    category: "schedule",
  });

  const [state, setState] = useState<any>();

  useEffect(() => {
    if (schedules && !loading) {
      const obj = [];
      schedules?.forEach((el) => {
        const findObj = obj?.find((item) => item.startDate === el?.startDate);
        const findCat = findObj?.catLessons.find(
          (item) => item.category.id === el.category._id
        );

        if (findObj) {
          if (findCat) {
            findCat?.schedule.push({
              lesson: { name: el?.lesson.name },
              times: el?.times,
            });
          } else {
            findObj?.catLessons.push({
              category: { id: el.category._id, name: el.category.name },
              schedule: [
                {
                  lesson: { name: el?.lesson.name },
                  times: el?.times,
                },
              ],
            });
          }
        } else {
          obj.push({
            startDate: el?.startDate,
            catLessons: [
              {
                category: { id: el.category._id, name: el.category.name },
                schedule: [
                  { lesson: { name: el?.lesson.name }, times: el?.times },
                ],
              },
            ],
          });
        }
      });
      setState(obj);
    }
  }, [schedules, loading]);

  return (
    <>
      {!gallery || (loading && <Loading />)}
      <CoverLayout src={`${process.env.API}${gallery?.images[0].path}`}>
        <Social />
      </CoverLayout>

      <div style={{ marginTop: "-3px" }}>
        <Breadcumb categories={breadCumbCats} />
      </div>
      <Section color="rgb(247, 247, 247)" padding="48px 0">
        <Container>
          <Grid container spacing={2}>
            {!loading && schedules?.length === 0 && <NoData />}
            {state &&
              state?.map((item) => (
                <Grid item xs={12} md={6} key={item._id}>
                  <Schedule data={item} />
                </Grid>
              ))}
          </Grid>
        </Container>
      </Section>
    </>
  );
};

export default SchedulesWidget;
