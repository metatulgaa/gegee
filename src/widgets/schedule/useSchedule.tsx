import { useEffect } from "react";
import { useState } from "react";
import { GetSchedulesQuery } from "../../graphql";
import sdk from "../../sdk";

export type Schedules = GetSchedulesQuery["getSchedules"];

const useSchedule = ({ categoryId }: { categoryId: string }) => {
  const [loading, setLoading] = useState(false);
  const [schedules, setSchedules] = useState<Schedules>();

  useEffect(() => {
    if (categoryId) {
      setLoading(true);
      sdk
        .getSchedules({ categoryId })
        .then((res) => setSchedules(res.getSchedules))
        .catch(() => {})
        .finally(() => setLoading(false));
    } else {
      setLoading(true);
      sdk
        .getSchedules()
        .then((res) => setSchedules(res.getSchedules))
        .catch(() => {})
        .finally(() => setLoading(false));
    }
  }, [categoryId]);

  return { loading, schedules };
};

export default useSchedule;
