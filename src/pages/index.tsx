import { useRouter } from "next/router";
import { GetServerSideProps } from "next";
import { Container, Grid } from "@material-ui/core";
import { Swiper, SwiperSlide } from "swiper/react";
import sdk from "../sdk";

import Card from "../components/Card";
import Social from "../components/Social";
import { homeMenu } from "../utils/menus";
import { Loading } from "../components/Loading";
import {
  CarouselBtn,
  CarouselButtonLayout,
  CarouselItem,
} from "../components/Carousel";
import { Section } from "../components/Section";

import useCategories from "../hooks/useCategories";
import { GetGalleryQuery } from "../graphql";

export const getServerSideProps: GetServerSideProps = async () => {
  const data = await sdk.getGallery({ category: "home" });
  return {
    props: {
      initialData: data.getGallery,
    },
  };
};

const HomePage = ({
  initialData,
}: {
  initialData: GetGalleryQuery["getGallery"];
}) => {
  const { loading, categories } = useCategories();
  const router = useRouter();
  return (
    <>
      {loading && initialData && <Loading />}
      <Social bottom={"40vh"} />

      <Swiper
        navigation
        pagination={{ clickable: true }}
        slidesPerView={1}
        parallax={true}
        speed={600}
      >
        {initialData.images.map((item) => (
          <SwiperSlide>
            <CarouselItem src={`${process.env.API}${item.path}`}>
              <CarouselButtonLayout justify="flex-end">
                <CarouselBtn onClick={() => router.push("/lessons")}>
                  Дэлгэрэнгүй
                  <i className="fa fa-external-link-alt" />
                </CarouselBtn>
              </CarouselButtonLayout>
            </CarouselItem>
          </SwiperSlide>
        ))}
      </Swiper>

      <Section padding="48px 0">
        <Container>
          <Grid container spacing={2}>
            {categories?.map((cat, index) => (
              <Grid item key={index} xs={6} sm={6} md={2}>
                <Card
                  imgPath={`${process.env.API}${cat.icon.path}`}
                  title={cat.name}
                  description={cat.tag}
                  border={true}
                  pathname={
                    cat.childrenCategories.length === 0
                      ? `/lessons/${cat._id}`
                      : `/lessons/child/${cat._id}`
                  }
                />
              </Grid>
            ))}
            {homeMenu.map((item, index) => (
              <Grid item key={index} xs={6} sm={6} md={2}>
                <Card
                  imgPath={item.imgPath}
                  title={item.title}
                  description={item.description}
                  border={true}
                  pathname={item.pathname}
                />
              </Grid>
            ))}
          </Grid>
        </Container>
      </Section>
    </>
  );
};

export default HomePage;
