import Social from "../../components/Social";
import TabMenu from "../../components/TabMenu";
import Breadcumb from "../../components/Breadcumb";
import { CoverLayout } from "../../components/Carousel";

import TestWidget from "../../widgets/test";

const breadCumbCats = [
  { index: 1, name: "Нүүр хуудас", pathname: "/" },
  { index: 2, name: "Түвшин тогтоох", pathname: "/test" },
];

const menus = [
  { index: 0, name: "Англи хэл" },
  { index: 1, name: "Хятад хэл" },
];

const OnlineTestPage = () => {
  return (
    <>
      <CoverLayout src="/assets/cover/test.jpg">
        <Social />
      </CoverLayout>
      <div style={{ marginTop: "-3px" }}>
        <Breadcumb categories={breadCumbCats} />
      </div>
      <TabMenu menus={menus} color="rgb(255, 166, 0)" selectKey={0}>
        {[<TestWidget />, <TestWidget />]}
      </TabMenu>
    </>
  );
};

export default OnlineTestPage;
