import { useState } from "react";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { Container, Grid } from "@material-ui/core";

import Card from "../../../components/Card";
import Social from "../../../components/Social";
import Breadcumb from "../../../components/Breadcumb";
import { Loading } from "../../../components/Loading";
import { Section } from "../../../components/Section";
import { TabLayout } from "../../../components/TabMenu";
import { CoverLayout } from "../../../components/Carousel";
import useCategories from "../../../hooks/useCategories";
import ChildLessonsWidget from "../../../widgets/lessons/child";
import ChildChildLessonWidget from "../../../widgets/lessons/childChild";

const breadCumbCats = [
  { index: 1, name: "Нүүр хуудас", pathname: "/" },
  { index: 2, name: "Сургалтууд", pathname: "/lessons" },
  { index: 3, name: "Хүүхдийн сургалтууд", pathname: "/lesson/child" },
];

const ChildPage = () => {
  const { categories, loading } = useCategories();

  const [menus, setMenus] = useState<any>();
  const [show, setShow] = useState<string>();

  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    if (categories && id) {
      const findedMenus = categories?.find((cat) => cat._id === id)
        ?.childrenCategories;

      setMenus(findedMenus);
      setShow(
        categories?.find((cat) => cat._id === id)?.childrenCategories[0]._id
      );
    }
  }, [categories, id]);

  return (
    <>
      {loading && <Loading />}

      <CoverLayout
        src={`${process.env.API}${
          categories?.find((item) => item._id === id)?.image?.path
        }`}
      >
        <Social />
      </CoverLayout>
      <div style={{ marginTop: "-3px" }}>
        <Breadcumb categories={breadCumbCats} />
      </div>
      <Container maxWidth="md">
        <TabLayout>
          {menus?.map((menu) => (
            <a
              className={show === menu._id && "active"}
              key={menu._id}
              onClick={() => setShow(menu._id)}
              style={{
                backgroundColor: show === menu._id && menu.color,
                width: `calc(100% / ${menus?.length})`,
              }}
            >
              {menu.name}
            </a>
          ))}
        </TabLayout>
      </Container>

      {menus?.find((item) => item._id === show).childrenCategories.length ===
      0 ? (
        <Section color="rgb(247,247,247)" padding="48px 0">
          <Container maxWidth="md">
            <ChildLessonsWidget
              categoryId={show}
              color={menus?.find((item) => item._id === show)?.color}
            />
          </Container>
        </Section>
      ) : (
        <>
          {menus
            ?.find((item) => item._id === show)
            ?.childrenCategories.map((child) => (
              <>
                <Section color="rgb(247,247,247)" padding="48px 0">
                  <Container maxWidth="md">
                    <ChildChildLessonWidget
                      categoryId={child._id}
                      name={child.name}
                      color={child.color}
                    />
                  </Container>
                </Section>
                <Section color="#fff" padding="2px"></Section>
              </>
            ))}
        </>
      )}

      <Section color="rgb(255,255,255)" padding="36px 0">
        <Container>
          <Grid container justify="center" spacing={2}>
            <Grid item xs={6} sm={2}>
              <Card
                border={true}
                title="Түвшин тогтоох"
                description="Онлайн тест"
                imgPath="/assets/icons/level.svg"
                pathname="/test"
              />
            </Grid>
            <Grid item xs={6} sm={2}>
              <Card
                border={true}
                title="Хичээлийн"
                description="хуваарь"
                imgPath="/assets/icons/schedule.svg"
                pathname={`/schedule/${id}`}
              />
            </Grid>
            <Grid item xs={2}>
              <Card
                border={true}
                title="Бүртгүүлэх"
                description="Сургалтад бүртгүүлэх"
                imgPath="/assets/icons/register.svg"
                pathname="https://docs.google.com/forms/d/1hlxA4ln1ZV7SXk_UT0pHasfQSSNtBV15lD829Ezn_A4/viewform?fbclid=IwAR3VTQIM3PgckDGN_h0LwJwv7x79ohMX8MEDiKjgWT7bynfoEoFIOXu2MjY&edit_requested=true"
              />
            </Grid>
          </Grid>
        </Container>
      </Section>
    </>
  );
};

export default ChildPage;
