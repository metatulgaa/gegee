import { GetServerSideProps } from "next";
import { Container, Grid } from "@material-ui/core";

import Card from "../../components/Card";
import Social from "../../components/Social";
import NoData from "../../components/NoData";
import Collapse from "../../components/Collapse";
import Breadcumb from "../../components/Breadcumb";
import { Loading } from "../../components/Loading";
import { Section } from "../../components/Section";
import { CoverLayout } from "../../components/Carousel";

import sdk from "../../sdk";
import useCategories from "../../hooks/useCategories";
import { LessonsList } from "../../widgets/lessons/useLessons";

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const data = await sdk.getLessons({ categoryId: ctx.params.id as string });
  return {
    props: {
      initialData: data.getLessons,
      categoryId: ctx.params.id as string,
    },
  };
};

const LessonsListPage = ({
  initialData,
  categoryId,
}: {
  initialData?: LessonsList;
  categoryId: string;
}) => {
  const { categories, loading: catLoad } = useCategories();

  const breadCumbCats = [
    { index: 1, name: "Нүүр хуудас", pathname: "/" },
    { index: 2, name: "Сургалтууд", pathname: "/lessons" },
    {
      index: 3,
      name: categories?.find((item) => item._id === categoryId)?.name,
      pathname: `/lessons/${categoryId}`,
    },
  ];

  return (
    <>
      {!initialData && <Loading />}
      <CoverLayout
        src={`${process.env.API}${
          categories?.find((item) => item._id === categoryId)?.image?.path
        }`}
      >
        <Social />
      </CoverLayout>
      {!catLoad && (
        <div style={{ marginTop: "-3px" }}>
          <Breadcumb categories={breadCumbCats} />
        </div>
      )}
      <Section padding="48px 0">
        <Container>
          <Grid container spacing={2}>
            {initialData?.length !== 0 ? (
              initialData?.map((item) => (
                <Grid item xs={12} key={item._id}>
                  <Collapse
                    index={item.index}
                    color="rgb(255, 166, 0)"
                    title={item.name}
                    description={item.description}
                    data={[
                      { title: "Хугацаа", value: item?.period.toString() },
                      {
                        title: "Хичээллэх өдөр",
                        value: item?.days.map((item) => `${item}, `),
                      },
                      {
                        title: "Хичээллэх цаг",
                        value: item?.studyTime.toString(),
                      },
                      {
                        title: "Нэг ангид",
                        value: item?.studentLimit.toString(),
                      },
                      {
                        title: "Хүрэх түвшин",
                        value: item.level,
                      },
                      { title: "Төлбөр", value: item?.price.toString() },
                    ]}
                  />
                </Grid>
              ))
            ) : (
              <NoData />
            )}
          </Grid>
        </Container>
      </Section>
      <Section color="rgb(247, 247, 247)" padding="48px 0">
        <Container>
          <Grid container spacing={2} justify="center">
            <Grid item xs={6} sm={2}>
              <Card
                border={true}
                title="Түвшин тогтоох"
                description="Онлайн тест"
                imgPath="/assets/icons/level.svg"
                pathname="/test"
              />
            </Grid>
            <Grid item xs={6} sm={2}>
              <Card
                border={true}
                title="Хичээлийн"
                description="хуваарь"
                imgPath="/assets/icons/schedule.svg"
                pathname={`/schedule/${categoryId}`}
              />
            </Grid>
            <Grid item xs={6} sm={2}>
              <Card
                border={true}
                title="Бүртгэх"
                description="Сургалтад бүртгүүлэх"
                imgPath="/assets/icons/level.svg"
                pathname="https://docs.google.com/forms/d/1NFPFny6SnxRasSQnfcAUlKPGby-JwRT2iYdO5Nmr5tE/viewform?ts=5fe32537&gxids=7628&fbclid=IwAR1_OzrrPsDMFibJubq1y_BDpkBERKgMbjknG9K68egXT3ovs5-_zR8x5cA&edit_requested=true"
              />
            </Grid>
          </Grid>
        </Container>
      </Section>
    </>
  );
};

export default LessonsListPage;
