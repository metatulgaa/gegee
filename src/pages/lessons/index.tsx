import { Container, Grid } from "@material-ui/core";

import Social from "../../components/Social";
import Card from "../../components/Card";
import Breadcumb from "../../components/Breadcumb";
import { CoverLayout } from "../../components/Carousel";
import { Loading } from "../../components/Loading";
import useCategories from "../../hooks/useCategories";
import useGallery from "../../hooks/useGallery";

const breadCumbCats = [
  { index: 1, name: "Нүүр хуудас", pathname: "/" },
  { index: 2, name: "Сургалтууд", pathname: "/lessons" },
];

const LessonsPage = () => {
  const { categories, loading } = useCategories();
  const { gallery } = useGallery({ category: "lesson" });
  return (
    <>
      <CoverLayout src={`${process.env.API}${gallery?.images[0]?.path}`}>
        <Social />
      </CoverLayout>
      <div style={{ marginTop: "-3px" }}>
        <Breadcumb categories={breadCumbCats} />
      </div>
      <div style={{ padding: "48px 0" }}>
        <Container maxWidth="md">
          <Grid container spacing={2} justify="center">
            {loading ? (
              <Loading />
            ) : (
              <>
                {categories?.map((item) => (
                  <Grid item key={item._id} xs={6} sm={6} md={3}>
                    <Card
                      imgPath={`${process.env.API}${item?.icon?.path}`}
                      title={item.name}
                      pathname={
                        item.childrenCategories.length === 0
                          ? `/lessons/${item._id}`
                          : `/lessons/child/${item._id}`
                      }
                      border={true}
                    />
                  </Grid>
                ))}
                <Grid item xs={6} sm={6} md={3}>
                  <Card
                    imgPath="/assets/icons/level.svg"
                    title="Түвшин тогтоох"
                    border={true}
                    pathname="/test"
                  />
                </Grid>
              </>
            )}
          </Grid>
        </Container>
      </div>
    </>
  );
};

export default LessonsPage;
