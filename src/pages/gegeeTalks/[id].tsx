import { GetServerSideProps } from "next";
import { Container, Grid } from "@material-ui/core";

import sdk from "../../sdk";
import Card from "../../components/Card";
import Social from "../../components/Social";
import Breadcumb from "../../components/Breadcumb";
import { Section } from "../../components/Section";
import { CoverLayout } from "../../components/Carousel";
import { GegeeTalksDetailWidget } from "../../widgets/gegeeTalks/detail";
import { GetArticleQuery } from "../../graphql";
import useGallery from "../../hooks/useGallery";
import { Loading } from "../../components/Loading";

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const data = await sdk.getArticle({ _id: ctx.params.id as string });
  return {
    props: {
      initialData: data.getArticle,
      id: ctx.params.id as string,
    },
  };
};

export type OneArticle = GetArticleQuery["getArticle"];

const GegeeTalksDetailtPage = ({
  initialData,
  id,
}: {
  initialData?: OneArticle;
  id: string;
}) => {
  const { gallery } = useGallery({ category: "talk" });

  const breadCumbCats = [
    { index: 1, name: "Нүүр хуудас", pathname: "/" },
    { index: 2, name: "Gegee Talks", pathname: "/gegeeTalks" },
    {
      index: 3,
      name: initialData.title,
      pathname: `/gegeeTalks/${id?.toString()}`,
    },
  ];
  return (
    <>
      {!gallery || (!initialData && <Loading />)}
      <CoverLayout src={`${process.env.API}${gallery?.images[0].path}`}>
        <Social />
      </CoverLayout>
      <div style={{ marginTop: "-3px" }}>
        <Breadcumb categories={breadCumbCats} />
      </div>
      <GegeeTalksDetailWidget initialData={initialData} />
      <Section padding="48px 0">
        <Container>
          <Grid container spacing={2} justify="center">
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Түвшин тогтоох"
                description="Онлайн тест"
                imgPath="/assets/icons/level.svg"
                pathname="/test"
              />
            </Grid>
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Хичээлийн"
                description="хуваарь"
                imgPath="/assets/icons/schedule.svg"
                pathname="/schedule"
              />
            </Grid>
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Бүртгүүлэх"
                description="Сургалтад бүртгүүлэх"
                imgPath="/assets/icons/register.svg"
                pathname="https://docs.google.com/forms/d/1NFPFny6SnxRasSQnfcAUlKPGby-JwRT2iYdO5Nmr5tE/viewform?ts=5fe32537&gxids=7628&fbclid=IwAR1_OzrrPsDMFibJubq1y_BDpkBERKgMbjknG9K68egXT3ovs5-_zR8x5cA&edit_requested=true"
              />
            </Grid>
          </Grid>
        </Container>
      </Section>
    </>
  );
};

export default GegeeTalksDetailtPage;
