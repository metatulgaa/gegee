import { Container, Grid } from "@material-ui/core";

import Breadcumb from "../../components/Breadcumb";
import Card from "../../components/Card";
import { CoverLayout } from "../../components/Carousel";
import { Loading } from "../../components/Loading";
import { Section } from "../../components/Section";
import Social from "../../components/Social";
import useGallery from "../../hooks/useGallery";
import { GegeeTalksWidget } from "../../widgets/gegeeTalks";

const breadCumbCats = [
  { index: 1, name: "Нүүр хуудас", pathname: "/" },
  { index: 2, name: "Gegee Talks", pathname: "/gegeeTalks" },
];

const GegeeTalksPage = () => {
  const { gallery } = useGallery({ category: "talk" });
  return (
    <>
      <CoverLayout src={`${process.env.API}${gallery?.images[0].path}`}>
        <Social />
      </CoverLayout>
      <div style={{ marginTop: "-3px" }}>
        <Breadcumb categories={breadCumbCats} />
      </div>
      <Section padding="48px 0">
        <Container maxWidth="md">
          <GegeeTalksWidget imageLoading={!gallery} />
        </Container>
      </Section>
      <Section padding="48px 0" color="rgb(247,247,247)">
        <Container>
          <Grid container spacing={2} justify="center">
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Түвшин тогтоох"
                description="Онлайн тест"
                imgPath="/assets/icons/level.svg"
                pathname="/test"
              />
            </Grid>
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Хичээлийн"
                description="хуваарь"
                imgPath="/assets/icons/schedule.svg"
                pathname="/schedule"
              />
            </Grid>
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Бүртгүүлэх"
                description="Сургалтад бүртгүүлэх"
                imgPath="/assets/icons/register.svg"
                pathname="https://docs.google.com/forms/d/1NFPFny6SnxRasSQnfcAUlKPGby-JwRT2iYdO5Nmr5tE/viewform?ts=5fe32537&gxids=7628&fbclid=IwAR1_OzrrPsDMFibJubq1y_BDpkBERKgMbjknG9K68egXT3ovs5-_zR8x5cA&edit_requested=true"
              />
            </Grid>
          </Grid>
        </Container>
      </Section>
    </>
  );
};

export default GegeeTalksPage;
