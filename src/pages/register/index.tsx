import { Container, Grid } from "@material-ui/core";

import Social from "../../components/Social";
import Breadcumb from "../../components/Breadcumb";
import { Section } from "../../components/Section";
import { CoverLayout } from "../../components/Carousel";
import useCategories from "../../hooks/useCategories";
import Card from "../../components/Card";
import { Loading } from "../../components/Loading";

const breadCumbCats = [
  { index: 1, name: "Нүүр хуудас", pathname: "/" },
  { index: 2, name: "Бүртгэлүүд", pathname: "/register" },
];

const RegisterPage = () => {
  const { loading, categories } = useCategories();
  return (
    <>
      {loading && <Loading />}
      <Social />
      <CoverLayout src="/assets/cover/home-2.jpg" />
      <div style={{ marginTop: "-3px" }}>
        <Breadcumb categories={breadCumbCats} />
      </div>
      <Section padding="48px 0">
        <Container maxWidth="md">
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <h3
                style={{
                  color: "orange",
                  textAlign: "center",
                  marginTop: 0,
                  textTransform: "uppercase",
                }}
              >
                Бүртгүүлэх хичээлээ сонгох
              </h3>
            </Grid>
            {categories?.map((cat, index) => (
              <Grid item key={index} xs={6} sm={4}>
                <Card
                  imgPath={"/assets/icons/english.svg"}
                  title={cat.name}
                  description={cat.tag}
                  border={true}
                  pathname="/register"
                />
              </Grid>
            ))}
          </Grid>
        </Container>
      </Section>
    </>
  );
};

export default RegisterPage;
