import { useRouter } from "next/router";
import { Container, Grid } from "@material-ui/core";

import Social from "../../components/Social";
import AboutWidget from "../../widgets/about";
import TabMenu from "../../components/TabMenu";
import Breadcumb from "../../components/Breadcumb";
import { Section } from "../../components/Section";
import { CoverLayout } from "../../components/Carousel";
import { AboutReasonWidget } from "../../widgets/about/reason";
import Card from "../../components/Card";
import useGallery from "../../hooks/useGallery";
import { Loading } from "../../components/Loading";

const breadCumbCats = [
  { index: 1, name: "Нүүр хуудас", pathname: "/" },
  { index: 2, name: "Яагаад гэгээ вэ?", pathname: "/about/0" },
];

const menus = [
  { index: 0, name: "Гэгээг сонгох шалтгаан" },
  { index: 1, name: "Бидний тухай" },
  { index: 2, name: "Хаяг байршил" },
];

const AboutPage = () => {
  const { gallery } = useGallery({ category: "reason" });
  const router = useRouter();
  const { number } = router.query;

  return (
    <>
      {!gallery && <Loading />}
      <CoverLayout
        style={{ height: "320px" }}
        src={`${process.env.API}${gallery?.images[0].path}`}
      >
        <Social />
      </CoverLayout>
      <div style={{ marginTop: "-3px" }}>
        <Breadcumb categories={breadCumbCats} />
      </div>
      {number && (
        <TabMenu
          menus={menus}
          color="rgb(255, 166, 0)"
          selectKey={Number(number)}
        >
          {[
            <AboutReasonWidget />,
            <AboutWidget />,
            <>
              <Section color="rgb(247,247,247)" padding="48px 0 0 0">
                <Container maxWidth="md">
                  <img src="/assets/about/location.jpg" width="100%" />
                </Container>
              </Section>
              <Section padding="48px 0">
                <Container>
                  <Grid container spacing={2} justify="center">
                    <Grid item xs={6} sm={4} md={2}>
                      <Card
                        border={true}
                        title="Түвшин тогтоох"
                        description="Онлайн тест"
                        imgPath="/assets/icons/level.svg"
                        pathname="/test"
                      />
                    </Grid>
                    <Grid item xs={6} sm={4} md={2}>
                      <Card
                        border={true}
                        title="Хичээлийн"
                        description="хуваарь"
                        imgPath="/assets/icons/schedule.svg"
                        pathname="/schedule"
                      />
                    </Grid>
                    <Grid item xs={6} sm={4} md={2}>
                      <Card
                        border={true}
                        title="Бүртгүүлэх"
                        description="Сургалтад бүртгүүлэх"
                        imgPath="/assets/icons/register.svg"
                        pathname="https://docs.google.com/forms/d/1NFPFny6SnxRasSQnfcAUlKPGby-JwRT2iYdO5Nmr5tE/viewform?ts=5fe32537&gxids=7628&fbclid=IwAR1_OzrrPsDMFibJubq1y_BDpkBERKgMbjknG9K68egXT3ovs5-_zR8x5cA&edit_requested=true"
                      />
                    </Grid>
                  </Grid>
                </Container>
              </Section>
            </>,
          ]}
        </TabMenu>
      )}
    </>
  );
};

export default AboutPage;
