import { Container, Grid } from "@material-ui/core";

import Card from "../../components/Card";
import Social from "../../components/Social";
import Breadcumb from "../../components/Breadcumb";
import { Section } from "../../components/Section";
import { AdviceWidget } from "../../widgets/advice";
import { CoverLayout } from "../../components/Carousel";
import useGallery from "../../hooks/useGallery";
import { Loading } from "../../components/Loading";

const breadCumbCats = [
  { index: 1, name: "Нүүр хуудас", pathname: "/" },
  { index: 2, name: "Зөвлөгөө", pathname: "/advice" },
];

const AdvicePage = () => {
  const { gallery } = useGallery({ category: "advice" });
  return (
    <>
      {!gallery && <Loading />}
      <CoverLayout src={`${process.env.API}${gallery?.images[0].path}`}>
        <Social />
      </CoverLayout>
      <div style={{ marginTop: "-3px" }}>
        <Breadcumb categories={breadCumbCats} />
      </div>
      <Section padding="48px 0">
        <Container maxWidth="md">
          <AdviceWidget />
        </Container>
      </Section>
      <Section padding="48px 0" color="rgb(247,247,247)">
        <Container>
          <Grid container spacing={2} justify="center">
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Түвшин тогтоох"
                description="Онлайн тест"
                imgPath="/assets/icons/level.svg"
                pathname="/test"
              />
            </Grid>
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Хичээлийн"
                description="хуваарь"
                imgPath="/assets/icons/schedule.svg"
                pathname="/schedule"
              />
            </Grid>
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Бүртгүүлэх"
                description="Сургалтад бүртгүүлэх"
                imgPath="/assets/icons/register.svg"
                pathname="https://docs.google.com/forms/d/1NFPFny6SnxRasSQnfcAUlKPGby-JwRT2iYdO5Nmr5tE/viewform?ts=5fe32537&gxids=7628&fbclid=IwAR1_OzrrPsDMFibJubq1y_BDpkBERKgMbjknG9K68egXT3ovs5-_zR8x5cA&edit_requested=true"
              />
            </Grid>
          </Grid>
        </Container>
      </Section>
    </>
  );
};

export default AdvicePage;
