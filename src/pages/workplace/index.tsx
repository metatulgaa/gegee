import { Container } from "@material-ui/core";

import Social from "../../components/Social";
import Breadcumb from "../../components/Breadcumb";
import { Section } from "../../components/Section";
import { CoverLayout } from "../../components/Carousel";

import { WorkplaceWidget } from "../../widgets/workplace";
import useGallery from "../../hooks/useGallery";

const breadCumbCats = [
  { index: 1, name: "Нүүр хуудас", pathname: "/" },
  { index: 2, name: "Ажлын байр", pathname: "/workplace" },
];

const WorkplacePage = () => {
  const { gallery } = useGallery({ category: "workplace" });
  return (
    <>
      <CoverLayout src={`${process.env.API}${gallery?.images[0].path}`}>
        <Social />
      </CoverLayout>
      <div style={{ marginTop: "-3px" }}>
        <Breadcumb categories={breadCumbCats} />
      </div>
      <Section padding="48px 0">
        <Container maxWidth="md">
          <WorkplaceWidget galleryLoading={!gallery} />
        </Container>
      </Section>
    </>
  );
};

export default WorkplacePage;
