import { Container, Grid } from "@material-ui/core";
import Card from "../../components/Card";

import { Section } from "../../components/Section";
import SchedulesWidget from "../../widgets/schedule";

const breadCumbCats = [
  { index: 1, name: "Нүүр хуудас", pathname: "/" },
  { index: 2, name: "Хичээлийн хуваариуд", pathname: "/schedule" },
];

const SchedulePage = () => {
  return (
    <>
      <SchedulesWidget breadCumbCats={breadCumbCats} />
      <Section padding="48px 0">
        <Container>
          <Grid container spacing={2} justify="center">
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Түвшин тогтоох"
                description="Онлайн тест"
                imgPath="/assets/icons/level.svg"
                pathname="/test"
              />
            </Grid>
            <Grid item xs={6} sm={4} md={2}>
              <Card
                border={true}
                title="Бүртгүүлэх"
                description="Сургалтад бүртгүүлэх"
                imgPath="/assets/icons/register.svg"
                pathname="https://docs.google.com/forms/d/1NFPFny6SnxRasSQnfcAUlKPGby-JwRT2iYdO5Nmr5tE/viewform?ts=5fe32537&gxids=7628&fbclid=IwAR1_OzrrPsDMFibJubq1y_BDpkBERKgMbjknG9K68egXT3ovs5-_zR8x5cA&edit_requested=true"
              />
            </Grid>
          </Grid>
        </Container>
      </Section>
    </>
  );
};

export default SchedulePage;
