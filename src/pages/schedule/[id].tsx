import { Container, Grid } from "@material-ui/core";
import { useRouter } from "next/router";

import Card from "../../components/Card";
import { Loading } from "../../components/Loading";
import { Section } from "../../components/Section";
import useCategories from "../../hooks/useCategories";
import SchedulesWidget from "../../widgets/schedule";

const LessonSchedulePage = () => {
  const { categories, loading } = useCategories();
  const router = useRouter();
  const { id } = router.query;

  const breadCumbCats = [
    { index: 1, name: "Нүүр хуудас", pathname: "/" },
    { index: 2, name: "Хичээлийн хуваариуд", pathname: "/schedule" },
    {
      index: 3,
      name: categories?.find((item) => item._id === id).name,
      pathname: "/schedule",
    },
  ];

  return (
    <>
      {loading && <Loading />}
      <SchedulesWidget
        categoryId={id?.toString()}
        breadCumbCats={breadCumbCats}
      />

      <Section padding="48px 0">
        <Container maxWidth="md">
          <Grid container spacing={2} justify="center">
            <Grid item xs={6} sm={4}>
              <Card
                border={true}
                title="Түвшин тогтоох"
                description="Онлайн тест"
                imgPath="/assets/icons/level.svg"
                pathname="/test"
              />
            </Grid>
            <Grid item xs={6} sm={4}>
              <Card
                border={true}
                title="Бүртгүүлэх"
                description="Сургалтад бүртгүүлэх"
                imgPath="/assets/icons/register.svg"
                pathname="https://docs.google.com/forms/d/1NFPFny6SnxRasSQnfcAUlKPGby-JwRT2iYdO5Nmr5tE/viewform?ts=5fe32537&gxids=7628&fbclid=IwAR1_OzrrPsDMFibJubq1y_BDpkBERKgMbjknG9K68egXT3ovs5-_zR8x5cA&edit_requested=true"
              />
            </Grid>
          </Grid>
        </Container>
      </Section>
    </>
  );
};

export default LessonSchedulePage;
