import "@/assets/index.css";
import "@/assets/fontawesome.css";
import "swiper/swiper.scss";
import "swiper/components/navigation/navigation.scss";
import "swiper/components/pagination/pagination.scss";
import "swiper/components/scrollbar/scrollbar.scss";

import Head from "next/head";
import router from "next/router";
import NProgress from "nprogress";
import { useEffect, useState } from "react";
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from "swiper";

import "nprogress/nprogress.css";
import Header from "../components/Header/Header";
import Footer from "../components/Footer";
import { Loading } from "../components/Loading";
import useCategories from "../hooks/useCategories";
import MobileHeader from "../components/Header/MobileHeader";
import MobileFooter from "../components/MobileFooter";

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

router.events.on("routeChangeStart", () => NProgress.start());
router.events.on("routeChangeComplete", () => NProgress.done());
router.events.on("routeChangeError", () => NProgress.done());

function App({ Component, pageProps }) {
  const [show, setShow] = useState();
  const { loading, categories } = useCategories();

  useEffect(() => {
    window.fbAsyncInit = function () {
      FB.init({
        xfbml: true,
        version: "v9.0",
      });
    };
    (function (d, s, id) {
      var js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
      fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");
  }, []);

  return (
    <>
      {loading && <Loading />}
      <Head>
        <link rel="icon" type="image/png" href="/logo.png" />
      </Head>
      <Header categories={categories} show={show} setShow={setShow} />
      <MobileHeader />
      <div onMouseEnter={() => setShow(undefined)} style={{ zIndex: 1 }}>
        <Component {...pageProps} />
      </div>
      <MobileFooter />
      <Footer />

      <div id="fb-root" />
      <div
        className="fb-customerchat"
        attribution="setup_tool"
        page_id="1602357433371784"
        theme_color="#FFA500"
        logged_in_greeting="Сайн байна уу? Танд юугаар туслах вэ?"
        logged_out_greeting="Сайн байна уу? Танд юугаар туслах вэ?"
      />
    </>
  );
}
export default App;
